package capstone.com.verve.View.PatientFragments

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.MedicalRecord

import capstone.com.verve.R
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.item_medicalhistory_profile.view.*
import org.jetbrains.anko.find

class ProfileMedicalHistoryFragment : Fragment() {

    lateinit var mMedHistViewHolder: FirebaseRecyclerAdapter<MedicalRecord, MedHistViewHolder>
    lateinit var mRecyclerView: RecyclerView
    lateinit var mDatabase: DatabaseReference
    private var auth: FirebaseAuth? = null
    internal var firebaseConnection = FirebaseConnection()

    var txt_treatmentname: TextView? = null
    var txt_prescriptions: TextView? = null
    var txt_consultingdoctor: TextView? = null
    var txt_date: TextView? = null
    var txt_time: TextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView = inflater.inflate(R.layout.fragment_profile_medical_history, container, false)
        mRecyclerView = rootView.findViewById(R.id.rv_medicalhistory)

        txt_treatmentname = rootView.findViewById(R.id.txt_treatmentname)
        txt_consultingdoctor = rootView.findViewById(R.id.txt_consultingdoctor)
        txt_date = rootView.findViewById(R.id.txt_date)
        txt_time = rootView.findViewById(R.id.txt_time)
        txt_prescriptions = rootView.findViewById(R.id.txt_prescriptions)

        return rootView
    }

    override fun onStart() {
        super.onStart()
        mMedHistViewHolder.startListening()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        mRecyclerView.hasFixedSize()
        var layoutManager = LinearLayoutManager(context)
        layoutManager.reverseLayout = true
        layoutManager.stackFromEnd = true
        mRecyclerView.layoutManager = layoutManager

        mDatabase = FirebaseDatabase.getInstance().reference
        val recordQuery = mDatabase.child("MedicalRecord").child(firebaseConnection.currentUser) //TODO: Database
        val recordOptions = FirebaseRecyclerOptions.Builder<MedicalRecord>().setQuery(recordQuery, MedicalRecord::class.java).build()

        mMedHistViewHolder = object : FirebaseRecyclerAdapter<MedicalRecord, MedHistViewHolder>(recordOptions) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedHistViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_medicalhistory_profile, parent, false)

                auth = FirebaseAuth.getInstance()

                return MedHistViewHolder(view)
            }

            override fun onBindViewHolder(holder: MedHistViewHolder, position: Int, model: MedicalRecord) {
                holder.bind(model)
            }
        }

        mRecyclerView.adapter = mMedHistViewHolder
    }

    class MedHistViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        fun bind(records: MedicalRecord) = with (itemView) {
            txt_treatmentname.text = records.treatment
            txt_prescriptions.text = records.prescriptions

            //TODO: get consulting doctor who added medical record, date and time of record added
            txt_consultingdoctor.text = records.doctorName
            txt_date.text = records.date
            txt_time.text = records.time
            txt_prescriptions.text = records.prescriptions
            txt_treatmentname.text = records.treatment
        }
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ProfileMedicalHistoryFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}
