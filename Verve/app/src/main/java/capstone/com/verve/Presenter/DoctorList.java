package capstone.com.verve.Presenter;

public class DoctorList {
    String firstname, middlename, lastname, specialty, uid;

    public DoctorList() {
    }

    public DoctorList(String firstname, String middlename, String lastname, String specialty, String uid) {
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
        this.specialty = specialty;
        this.uid = uid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
