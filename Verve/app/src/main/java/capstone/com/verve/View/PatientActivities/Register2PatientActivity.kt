package capstone.com.verve.View.PatientActivities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.Registration
import capstone.com.verve.R

class Register2PatientActivity : AppCompatActivity() {

    var etxt_password: EditText? = null
    var etxt_confirmpass: EditText? = null
    var firstname: String? = null
    var middlename: String? = null
    var lastname: String? = null
    var username: String? = null
    var age: String? = null
    var mobilenum: String? = null
    var userEmail: String? = null
    var address: String? = null
    var birthdate: String? = null
    var gender: String? = null
    var religion: String? = null

    var btn_register: Button? = null

    var register = Registration()
    var firebaseConnection = FirebaseConnection()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register2_patient)

        var extras = intent.extras

        firstname = extras.getString("firstname")
        middlename = extras.getString("middlename")
        lastname = extras.getString("lastname")
        username = extras.getString("username")
        age = extras.getString("age")
        mobilenum = extras.getString("mobilenum")
        userEmail = extras.getString("userEmail")
        address = extras.getString("address")
        birthdate = extras.getString("birthdate")
        gender = extras.getString("gender")
        religion = extras.getString("religion")

        etxt_password = findViewById(R.id.et_password)
        etxt_confirmpass = findViewById(R.id.et_confirmpassword)
        btn_register = findViewById(R.id.btn_register)

        btn_register?.setOnClickListener{
            register.userAndEmailAuth(userEmail, etxt_password!!.text.toString(), firstname, middlename, lastname, username, mobilenum, address,birthdate, age,
                gender, "Patient", this@Register2PatientActivity, firebaseConnection.firebaseAuth, firebaseConnection.firebaseUser, religion)
        }
    }
}
