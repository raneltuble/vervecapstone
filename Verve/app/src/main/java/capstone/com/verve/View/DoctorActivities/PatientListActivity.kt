package capstone.com.verve.View.DoctorActivities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.*
import capstone.com.verve.R
import capstone.com.verve.View.DirectMessages.LatestMessagesActivity
import capstone.com.verve.View.PatientActivities.ForumActivity
import capstone.com.verve.View.PatientActivities.LoginActivity
import capstone.com.verve.View.PatientActivities.ProfileActivity
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.profile.profile
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.item_doctor_patientlist.view.*
import kotlinx.android.synthetic.main.item_posts_comments.view.*

class PatientListActivity : AppCompatActivity() {

    internal var firebaseConnection = FirebaseConnection()
    var wholeName: String = ""
    var userEmail: String = ""
    var patientId: String? = null
    var patientName: TextView? = null
    var patientFinding: TextView? = null
    var patientTreatment: TextView? = null
    var patientIcon: ImageView? = null
    var doctorForum: ImageView? = null
    var doctorProfile: ImageView? = null
    var doctorMessages: ImageView? = null
    var itemCard: CardView? = null
    var currUser: String? = null

    var userStatus = UserStatus()

    private var recyclerView: RecyclerView? = null
    lateinit var dDatabase: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patient_list)


        val extras = intent.extras
        wholeName = extras.getString("wholeName")
        userEmail = extras.getString("userEmail")

        recyclerView = findViewById(R.id.rv_patientlist)
        doctorForum = findViewById(R.id.img_forum)
        doctorProfile = findViewById(R.id.img_profile)
        doctorMessages = findViewById(R.id.img_messages)

        currUser = firebaseConnection.currentUser

        PatientsRecyclerView()

        doctorForum?.setOnClickListener {
            showForum()
        }

        doctorProfile?.setOnClickListener {
            showDoctorProfile()
        }

        doctorMessages?.setOnClickListener {
            showMessages()
        }

        leftMenu()
    }

    override fun onResume() {
        super.onResume()
        if (firebaseConnection.currentUser != null) {
            userStatus.updateDoctorStatus("Online")
        }

    }

    override fun onPause() {
        super.onPause()
        userStatus.updateDoctorStatus("Offline")
    }

    private fun leftMenu() {
        drawer {
            selectedItemByPosition = 5
            accountHeader {
                profile(wholeName!!, userEmail) {
                    icon = R.drawable.imgprofilepic
                }
                background = R.drawable.bgaccountheader
            }


            primaryItem("EVENTS") {
                icon = R.mipmap.ic_eventsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showEvents()
                    Log.d("DRAWER", "Events Activity")
                    true
                }
            }

            primaryItem("ABOUT") {
                icon = R.mipmap.ic_helpicon
                textColorRes = R.color.White
                onClick { _ ->
                    showAbout()
                    Log.d("DRAWER", "About Activity/Fragment")
                    true
                }
            }

            primaryItem("LOGOUT") {
                icon = R.mipmap.ic_logouticon
                textColorRes = R.color.White
                onClick { _ ->
                    logoutUser()
                    Log.d("DRAWER", "User Logout")
                    true
                }
            }

            sliderBackground = R.drawable.bgslider
        }
    }

    private fun logoutUser() {
        userStatus.updateDoctorStatus("Offline")
        firebaseConnection.userSignout()
        val intent = Intent(this@PatientListActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)


        return
    }

    private fun showAbout() {
        val intent = Intent(this@PatientListActivity, DoctorAboutActivity::class.java)
        startActivity(intent)
    }

    private fun showEvents() {
        val intent = Intent(this@PatientListActivity, DoctorEventsActivity::class.java)
        startActivity(intent)
    }

    private fun showForum() {
        val intent = Intent(this@PatientListActivity, DoctorForumActivity::class.java)
        var extras = Bundle()
        extras.putString("userEmail", userEmail)
        extras.putString("wholeName", wholeName)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showDoctorProfile() {
        val intent = Intent(this@PatientListActivity, DoctorProfileActivity::class.java)
        var extras = Bundle()
        extras.putString("userEmail", userEmail)
        extras.putString("wholeName", wholeName)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showMessages() {
        val intent = Intent(this@PatientListActivity, LatestMessagesActivity::class.java)
        var extras = Bundle()
        extras.putString("userEmail", userEmail)
        extras.putString("wholeName", wholeName)
        intent.putExtras(extras)
        startActivity(intent)
    }



    fun PatientsRecyclerView() {
        dDatabase = FirebaseDatabase.getInstance().reference
        val patientListQuery = dDatabase
            .child("PatientList").child(currUser!!)

        val patientListOptions = FirebaseRecyclerOptions.Builder<PatientList>()
            .setQuery(patientListQuery, PatientList::class.java)
            .setLifecycleOwner(this)
            .build()

        var layoutManager = LinearLayoutManager(this@PatientListActivity)
        recyclerView?.layoutManager = layoutManager

        var mPatientListViewHolder =
            object : FirebaseRecyclerAdapter<PatientList, PatientListViewHolder>(patientListOptions) {
                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PatientListViewHolder {
                    val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_doctor_patientlist, parent, false)

                    patientName = view.findViewById(R.id.patientlist_name)
                    patientFinding = view.findViewById(R.id.patientlist_findings)
                    patientTreatment = view.findViewById(R.id.patientlist_treatment)
                    patientIcon = view.findViewById(R.id.img_patient)
                    itemCard = view.findViewById(R.id.card_patient)


                    return PatientListViewHolder(view)
                }

                override fun onBindViewHolder(holder: PatientListViewHolder, position: Int, model: PatientList) {
                    holder.bind(model)

                    itemCard?.setOnClickListener {
                        patientId = getRef(holder.adapterPosition).key!!
                        // Toast.makeText(this@PatientListActivity, wholeName, Toast.LENGTH_LONG).show()
                        showPatientProfile()
                    }
                }

            }

        recyclerView?.adapter = mPatientListViewHolder
    }

    class PatientListViewHolder(val cView: View, var patients: PatientList? = null) : RecyclerView.ViewHolder(cView) {

        fun bind(patients: PatientList) = with(patients) {

            cView.patientlist_name.text = patients.firstname
            cView.patientlist_findings.text = patients.cancerType //TODO: QUERY
            cView.patientlist_treatment.text = patients.treatment //TODO: QUERY
        }
    }

    fun showPatientProfile() {
        val intent = Intent(this@PatientListActivity, PatientProfileActivity::class.java)
        var extras = Bundle()
        extras.putString("patientId", patientId)
        extras.putString("userEmail", userEmail)
        extras.putString("wholeName", wholeName)
        intent.putExtras(extras)
        startActivity(intent)
    }

}
