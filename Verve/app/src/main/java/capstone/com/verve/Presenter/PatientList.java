package capstone.com.verve.Presenter;

public class PatientList {
    String firstname;
    String middlename;
    String lastname;

    String treatment;
    String cancerType;

    public PatientList() {
    }

    public PatientList(String firstname, String middlename, String lastname, String treatment, String cancerType) {
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
        this.treatment = treatment;
        this.cancerType = cancerType;
    }

    public PatientList(String firstname, String middlename, String lastname) {
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public String getCancerType() {
        return cancerType;
    }

    public void setCancerType(String cancerType) {
        this.cancerType = cancerType;
    }


}
