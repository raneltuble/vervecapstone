package capstone.com.verve.View.PatientActivities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.util.Log
import android.widget.ImageView
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Interface.AcceptListener
import capstone.com.verve.R
import capstone.com.verve.View.PatientAdapters.MedRemindersPagerAdapter
import capstone.com.verve.View.PatientFragments.PatientContraindicationsFragment
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.profile.profile
import kotlinx.android.synthetic.main.activity_med_reminders.*

class MedRemindersActivity : BaseView(), AcceptListener {

    var wholeName: String = ""
    var userEmail: String = ""
    var userId: String = ""
    internal var firebaseConnection = FirebaseConnection()

    var profile: ImageView? = null
    var find: ImageView? = null
    var home: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_med_reminders)

        var extras = intent.extras

        wholeName = extras.getString("wholeName")
        userEmail = extras.getString("userEmail")
        userId = extras.getString("userId")

        profile = findViewById(R.id.img_profile)
        find = findViewById(R.id.img_find)
        home = findViewById(R.id.img_home)

        profile?.setOnClickListener {
            showProfile()
        }

        find?.setOnClickListener {
            showFind()
        }

        home?.setOnClickListener {
            showForum()
        }

        leftMenu()
        bindViewAndAdapter()
    }

    private fun bindViewAndAdapter() {
        val pageAdapter = MedRemindersPagerAdapter(supportFragmentManager, 2)
        viewpager.adapter = pageAdapter
        viewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
    }


    private fun leftMenu() {
        drawer {
            selectedItemByPosition = 9

            accountHeader {
                profile(wholeName, userEmail) {
                    icon = R.drawable.imgprofilepic
                }
                background = R.drawable.bgaccountheader
            }

            primaryItem("HEALTH TIPS") {
                icon = R.mipmap.ic_tipsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showHealthTips()
                    Log.d("DRAWER", "Health Tips Activity")
                    true
                }
            }

            primaryItem("EVENTS") {
                icon = R.mipmap.ic_eventsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showEvents()
                    Log.d("DRAWER", "Events Activity")
                    true
                }
            }

            primaryItem("ABOUT") {
                icon = R.mipmap.ic_helpicon
                textColorRes = R.color.White
                onClick { _ ->
                    showAbout()
                    Log.d("DRAWER", "About Activity/Fragment")
                    true
                }
            }

            primaryItem("MANAGE PRIVACY") {
                icon = R.mipmap.ic_settings
                textColorRes = R.color.White
                onClick { _ ->
                    showManagePrivacy()
                    Log.d("DRAWER", "Manage Privacy Activity")
                    true
                }
            }

            primaryItem("LOGOUT") {
                icon = R.mipmap.ic_logouticon
                textColorRes = R.color.White
                onClick { _ ->
                    logoutUser()
                    Log.d("DRAWER", "User Logout")
                    true
                }
            }

            sliderBackground = R.drawable.bgslider
        }
    }

    private fun logoutUser() {
        firebaseConnection.firebaseAuth.signOut()
        val intent = Intent(this@MedRemindersActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    private fun showManagePrivacy() {
        val intent = Intent(this@MedRemindersActivity, ManagePrivacyActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showAbout() {
        val intent = Intent(this@MedRemindersActivity, PatientAboutActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showEvents() {
        //val intent = Intent(this@ForumActivity, PatientEventsActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)

        val intent = Intent(this@MedRemindersActivity, PatientEventsActivity::class.java)
        startActivity(intent)
    }

    private fun showHealthTips() {
        val intent = Intent(this@MedRemindersActivity, PatientHealthTipsActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showForum() {
        val intent = Intent(this@MedRemindersActivity, ForumActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showProfile() {
        val intent = Intent(this@MedRemindersActivity, ProfileActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showFind() {
        val intent = Intent(this@MedRemindersActivity, FindActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    override fun onSubmit() {
    }

}
