package capstone.com.verve.Presenter;

public class Privacy {
    String address, age, bday, email, gender, icon, mobilenum, religion;

    public Privacy() {
    }

    public Privacy(String address, String age, String bday, String email, String gender, String icon, String mobilenum, String religion) {
        this.address = address;
        this.age = age;
        this.bday = bday;
        this.email = email;
        this.gender = gender;
        this.icon = icon;
        this.mobilenum = mobilenum;
        this.religion = religion;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBday() {
        return bday;
    }

    public void setBday(String bday) {
        this.bday = bday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMobilenum() {
        return mobilenum;
    }

    public void setMobilenum(String mobilenum) {
        this.mobilenum = mobilenum;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }
}
