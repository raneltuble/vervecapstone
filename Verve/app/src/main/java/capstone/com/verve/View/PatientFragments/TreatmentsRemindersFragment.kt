package capstone.com.verve.View.PatientFragments

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.TreatmentReminders

import capstone.com.verve.R
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.item_reminders_treatments.view.*
import org.jetbrains.anko.find

class TreatmentsRemindersFragment : Fragment() {

    lateinit var mTreatmentViewHolder: FirebaseRecyclerAdapter<TreatmentReminders, TreatmentViewHolder>
    lateinit var mRecyclerView: RecyclerView
    lateinit var mDatabase: DatabaseReference
    private var auth: FirebaseAuth? = null
    internal var firebaseConnection = FirebaseConnection()

    //EditText
    var txt_treatmentname: TextView? = null
    var txt_doctorname: TextView? = null
    var txt_day: TextView? = null
    var txt_time: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView = inflater.inflate(R.layout.fragment_treatments_reminders, container, false)
        mRecyclerView = rootView.findViewById(R.id.rv_treatmentrem)

        txt_treatmentname = rootView.findViewById(R.id.txt_treatmentname)
        txt_day = rootView.findViewById(R.id.txt_dayofmonth)
        txt_doctorname = rootView.findViewById(R.id.txt_doctorname)
        txt_time = rootView.findViewById(R.id.txt_time)

        return rootView
    }

    override fun onStart() {
        super.onStart()
        mTreatmentViewHolder.startListening()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mRecyclerView.hasFixedSize()
        var layoutManager = LinearLayoutManager(context)
        layoutManager.reverseLayout = true
        layoutManager.stackFromEnd = true
        mRecyclerView.layoutManager = layoutManager

        mDatabase = FirebaseDatabase.getInstance().reference
        val treatmentsQuery = mDatabase.child("TreatmentReminder").child(firebaseConnection.currentUser)//TODO: Database
        val treatmentsOptions = FirebaseRecyclerOptions.Builder<TreatmentReminders>()
            .setQuery(treatmentsQuery, TreatmentReminders::class.java).build()

        mTreatmentViewHolder =
            object : FirebaseRecyclerAdapter<TreatmentReminders, TreatmentViewHolder>(treatmentsOptions) {
                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TreatmentViewHolder {
                    val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_reminders_treatments, parent, false)

                    auth = FirebaseAuth.getInstance()

                    return TreatmentViewHolder(view)
                }

                override fun onBindViewHolder(holder: TreatmentViewHolder, position: Int, model: TreatmentReminders) {
                    holder.bind(model)
                }
            }

        mRecyclerView.adapter = mTreatmentViewHolder
    }

    class TreatmentViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        fun bind(treatments: TreatmentReminders) = with(itemView) {
            //TODO: MODEL
            txt_treatmentname?.text = treatments.treatmentName
            txt_doctorname?.text = treatments.doctorName
            txt_time?.text = treatments.time
            txt_day?.text = treatments.date

        }
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TreatmentsRemindersFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}
