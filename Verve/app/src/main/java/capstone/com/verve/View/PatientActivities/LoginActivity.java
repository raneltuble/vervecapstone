package capstone.com.verve.View.PatientActivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import capstone.com.verve.API.FirebaseConnection;
import capstone.com.verve.Presenter.Login;
import capstone.com.verve.Presenter.PatientList;
import capstone.com.verve.Presenter.UserPosts;
import capstone.com.verve.R;
import capstone.com.verve.View.DoctorActivities.PatientListActivity;
import capstone.com.verve.View.DoctorActivities.PatientProfileActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.*;

public class LoginActivity extends AppCompatActivity {
    Login login = new Login();
    String firstname = "";
    String middlename = "";
    String lastname = "";
    String wholeName = "";
    EditText etUsername, etPassword;
    FirebaseAuth auth;
    String patientRole = "Patient";
    String doctorRole = "Doctor";
    DatabaseReference db;
    String userEmail = "";

    Bundle extras = new Bundle();

    FirebaseUser user;

    FirebaseConnection firebaseConnection = new FirebaseConnection();

    Boolean emailChecker = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        etUsername = findViewById(R.id.etxt_username);
        etPassword = findViewById(R.id.etxt_password);

        user = firebaseConnection.getFirebaseUser();

        auth = firebaseConnection.getFirebaseAuth();

        db = firebaseConnection.getFirebaseDatabaseReference();

        if (user != null) {
            db.child("Users").child(user.getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String role = dataSnapshot.child("role").getValue(String.class);
                        firstname = dataSnapshot.child("firstname").getValue(String.class);
                        middlename = dataSnapshot.child("middlename").getValue(String.class);
                        lastname = dataSnapshot.child("lastname").getValue(String.class);
                        userEmail = dataSnapshot.child("email").getValue(String.class);
                        if (role.equals(patientRole)) {
                            if (middlename.isEmpty()) {
                                wholeName = firstname.concat(" " + lastname);
                            } else {
                                wholeName = firstname.concat(" " + middlename.charAt(0) + ".").concat(" " + lastname);
                            }
                            sendToPatientHome();
                        } else if (role.equals(doctorRole)) {
                            if (middlename.isEmpty()) {
                                wholeName = firstname.concat(" " + lastname);
                            } else {
                                wholeName = firstname.concat(" " + middlename.charAt(0) + ".").concat(" " + lastname);
                            }

                            sendToDoctorHome();
                        }


                    } else {

                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        } else {
            // User is signed out

        }

        setHintTextColor();
    }

    public void option(View v) {
        Intent i = null, chooser = null;

        if (v.getId() == R.id.click_signup) {
            i = new Intent(this, RegisterPatientActivity.class);
            startActivity(i);
        }

        if (v.getId() == R.id.click_forgotpass) {
            i = new Intent(this, ForgotPasswordActivity.class);
            startActivity(i);
        }

        if (v.getId() == R.id.btn_login) {

            login.allowUserToLogin(etUsername, etPassword, LoginActivity.this, auth, user);
        }
    }

    private void setHintTextColor() {
        EditText etUsername = findViewById(R.id.etxt_username);
        EditText etPassword = findViewById(R.id.etxt_password);

        etUsername.setHintTextColor(getResources().getColor(R.color.Pale));
        etPassword.setHintTextColor(getResources().getColor(R.color.Pale));
    }

    private void sendToPatientHome() {

        Intent intent = new Intent(LoginActivity.this, ForumActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        extras.putString("wholeName", wholeName);
        extras.putString("userEmail", userEmail);
        extras.putString("userId", firebaseConnection.getCurrentUser());
        intent.putExtras(extras);
        startActivity(intent);
    }

    private void sendToDoctorHome() {
        Intent intent = new Intent(LoginActivity.this, PatientListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        extras.putString("wholeName", wholeName);
        extras.putString("userEmail", userEmail);
        extras.putString("userId", firebaseConnection.getCurrentUser());
        intent.putExtras(extras);
        startActivity(intent);
    }
//    private void DisplayUserPosts(){
//        FirebaseRecyclerAdapter<Posts, >
//    }

}
