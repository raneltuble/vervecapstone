package capstone.com.verve.View.PatientActivities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.NonNull
import android.util.Log
import android.widget.ImageView
import android.widget.Switch
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.ManagePrivacy
import capstone.com.verve.R
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.profile.profile
import com.google.android.gms.tasks.Task
import org.jetbrains.anko.find

class ManagePrivacyActivity : AppCompatActivity() {

    var switch_gender: Switch? = null
    var switch_age: Switch? = null
    var switch_mobilenum: Switch? = null
    var switch_address: Switch? = null
    var switch_bday: Switch? = null
    var switch_email: Switch? = null
    var switch_icon: Switch? = null
    var switch_religion: Switch? = null

    var profile: ImageView? = null
    var find: ImageView? = null
    var forum: ImageView? = null
    var reminders: ImageView? = null
    var messages: ImageView? = null

    var managePrivacy = ManagePrivacy()

    internal var firebaseConnection = FirebaseConnection()

    //Strings

    var wholeName: String = ""
    var userEmail: String = ""
    var userId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_privacy)

        var extras = intent.extras

        wholeName = extras.getString("wholeName")
        userEmail = extras.getString("userEmail")
        userId = extras.getString("userId")

        switch_gender = findViewById(R.id.switch_gender)
        switch_age = findViewById(R.id.switch_age)
        switch_mobilenum = findViewById(R.id.switch_mobilenum)
        switch_address = findViewById(R.id.switch_address)
        switch_email = findViewById(R.id.switch_email)
        switch_icon = findViewById(R.id.switch_icon)
        switch_religion = findViewById(R.id.switch_religion)

        profile = findViewById(R.id.img_profile)
        find = findViewById(R.id.img_find)
        forum = findViewById(R.id.img_home)
        reminders = findViewById(R.id.img_reminders)
        messages = findViewById(R.id.img_messages)

        profile?.setOnClickListener {
            showProfile()
        }

        find?.setOnClickListener {
            showFind()
        }

        forum?.setOnClickListener {
            showForum()
        }

        reminders?.setOnClickListener {
            showReminders()
        }

        leftMenu()

        managePrivacy.setChecked(switch_age, switch_address,
            switch_bday, switch_email,
            switch_gender, switch_icon,
            switch_religion, switch_mobilenum)

        switch_gender?.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                managePrivacy.updatePrivacy("gender", "true")
            } else {
                managePrivacy.updatePrivacy("gender", "false")
            }
        }

        switch_age?.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                managePrivacy.updatePrivacy("age", "true")
                managePrivacy.updatePrivacy("bday", "true")
            } else {
                managePrivacy.updatePrivacy("age", "false")
                managePrivacy.updatePrivacy("bday", "false")
            }
        }

        switch_email?.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                managePrivacy.updatePrivacy("email", "true")
            } else {
                managePrivacy.updatePrivacy("email", "false")
            }
        }

        switch_mobilenum?.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                managePrivacy.updatePrivacy("mobilenum", "true")
            } else {
                managePrivacy.updatePrivacy("mobilenum", "false")
            }
        }

        switch_address?.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                managePrivacy.updatePrivacy("address", "true")
            } else {
                managePrivacy.updatePrivacy("address", "false")
            }
        }
        switch_religion?.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                managePrivacy.updatePrivacy("religion", "true")
            } else {
                managePrivacy.updatePrivacy("religion", "false")
            }
        }

        switch_icon?.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                managePrivacy.updatePrivacy("icon", "true")
            } else {
                managePrivacy.updatePrivacy("icon", "false")
            }
        }

    }

    override fun onStart() {
        super.onStart()

    }

    private fun leftMenu() {
        drawer {
            selectedItemByPosition = 9

            accountHeader {
                profile(wholeName, userEmail) {
                    icon = R.drawable.imgprofilepic
                }
                background = R.drawable.bgaccountheader
            }

            primaryItem("HEALTH TIPS") {
                icon = R.mipmap.ic_tipsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showHealthTips()
                    Log.d("DRAWER", "Health Tips Activity")
                    true
                }
            }

            primaryItem("EVENTS") {
                icon = R.mipmap.ic_eventsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showEvents()
                    Log.d("DRAWER", "Events Activity")
                    true
                }
            }

            primaryItem("ABOUT") {
                icon = R.mipmap.ic_helpicon
                textColorRes = R.color.White
                onClick { _ ->
                    showAbout()
                    Log.d("DRAWER", "About Activity/Fragment")
                    true
                }
            }

            primaryItem("MANAGE PRIVACY") {
                icon = R.mipmap.ic_settings
                textColorRes = R.color.White
                onClick { _ ->
                    showManagePrivacy()
                    Log.d("DRAWER", "Manage Privacy Activity")
                    true
                }
            }

            primaryItem("LOGOUT") {
                icon = R.mipmap.ic_logouticon
                textColorRes = R.color.White
                onClick { _ ->
                    logoutUser()
                    Log.d("DRAWER", "User Logout")
                    true
                }
            }

            sliderBackground = R.drawable.bgslider
        }
    }

    private fun logoutUser() {
        firebaseConnection.firebaseAuth.signOut()
        val intent = Intent(this@ManagePrivacyActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    private fun showAbout() {
        val intent = Intent(this@ManagePrivacyActivity, PatientAboutActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showManagePrivacy() {
        val intent = Intent(this@ManagePrivacyActivity, ManagePrivacyActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showEvents() {

        //val intent = Intent(this@ForumActivity, PatientEventsActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)

        val intent = Intent(this@ManagePrivacyActivity, PatientEventsActivity::class.java)

        startActivity(intent)
    }

    private fun showHealthTips() {
        val intent = Intent(this@ManagePrivacyActivity, PatientHealthTipsActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showProfile() {
        val intent = Intent(this@ManagePrivacyActivity, ProfileActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showFind() {
        val intent = Intent(this@ManagePrivacyActivity, FindActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showForum() {
        val intent = Intent(this@ManagePrivacyActivity, ForumActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showReminders() {
        val intent = Intent(this@ManagePrivacyActivity, MedRemindersActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }
}
