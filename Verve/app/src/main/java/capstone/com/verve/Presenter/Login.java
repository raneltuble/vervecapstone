package capstone.com.verve.Presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.EditText;
import android.widget.Toast;
import capstone.com.verve.API.FirebaseConnection;
import capstone.com.verve.API.Security;
import capstone.com.verve.View.DoctorActivities.PatientListActivity;
import capstone.com.verve.View.PatientActivities.ForumActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.*;
import com.scottyab.aescrypt.AESCrypt;

import java.security.GeneralSecurityException;

public class Login {
    Users users = new Users();
    String wholeName = "";
    String firstname = "";
    String middlename = "";
    String lastname = "";
    String userEmail = "";
    String patientRole = "Patient";
    String doctorRole = "Doctor";
    Boolean emailAddressChecker = false;
    Security sec = new Security();
    String decryptRole = "";

    FirebaseConnection firebaseConnection = new FirebaseConnection();


    Bundle extras = new Bundle();


    public void allowUserToLogin(EditText email, EditText password, Context context, FirebaseAuth auth, FirebaseUser user) {
        String emailAdd = email.getText().toString().trim();
        String userPassword = password.getText().toString().trim();

        if (emailAdd.isEmpty()) {
            Toast.makeText(context, "Please Enter your email", Toast.LENGTH_LONG).show();
            return;
        }
        if (userPassword.isEmpty()) {
            Toast.makeText(context, "Please Enter you password", Toast.LENGTH_LONG).show();
            return;
        }


        userLogin(emailAdd, userPassword, context, auth, user);

    }


    private void userLogin(String email, String password, final Context context, final FirebaseAuth auth,
                           final FirebaseUser user) {


        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    mRef.child("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                String role = dataSnapshot.child("role").getValue(String.class);
                                firstname = dataSnapshot.child("firstname").getValue(String.class);
                                middlename = dataSnapshot.child("middlename").getValue(String.class);
                                lastname = dataSnapshot.child("lastname").getValue(String.class);
                                userEmail = dataSnapshot.child("email").getValue(String.class);
                                try{
                                    decryptRole = AESCrypt.decrypt(sec.setSecurityKey(), role);
                                } catch (GeneralSecurityException e) {
                                    e.printStackTrace();
                                }
                                if (role.equals(patientRole)) {
                                    if (middlename.isEmpty()){
                                        wholeName = firstname.concat(" " + lastname);
                                    }else{
                                         wholeName = firstname.concat(" " + middlename.charAt(0) + ".").concat(" " + lastname);
                                    }

                                    sendPatientToHome(context, firebaseConnection.getCurrentUser());
                                }else if (role.equals(doctorRole)){
                                    if (middlename.isEmpty()){
                                        wholeName = firstname.concat(" " + lastname);
                                    }else{
                                        wholeName = firstname.concat(" " + middlename.charAt(0) + ".").concat(" " + lastname);
                                    }
                                    sendDoctorToHome(context,firebaseConnection.getCurrentUser());
                                }
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                } else {
                    Toast.makeText(context, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void sendPatientToHome(Context context, String userId) {
        Intent intent = new Intent(context, ForumActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        extras.putString("wholeName", wholeName);
        extras.putString("userEmail", userEmail);
        extras.putString("userId", userId);
        intent.putExtras(extras);
        context.startActivity(intent);

    }


    private void sendDoctorToHome(Context context, String userId) {
        Intent intent = new Intent(context, PatientListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        extras.putString("wholeName", wholeName);
        extras.putString("userEmail", userEmail);
        //extras.putString("userId", userId);
        intent.putExtras(extras);
        context.startActivity(intent);

    }


//    private void checkIfEmailIsVerified(Context context, FirebaseAuth auth, FirebaseUser user) {
//        //FirebaseUser user = auth.getCurrentUser();
//        emailAddressChecker = user.isEmailVerified();
//
//        if (emailAddressChecker) {
//            sendPatientToForum(context);
//        } else {
//            Toast.makeText(context, "Please Verify your account first", Toast.LENGTH_SHORT).show();
//            auth.signOut();
//        }
//    }
}


