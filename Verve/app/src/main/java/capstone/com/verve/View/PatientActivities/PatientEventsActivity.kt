package capstone.com.verve.View.PatientActivities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.R
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.profile.profile

class PatientEventsActivity : AppCompatActivity() {

    var profile: ImageView? = null
    var find: ImageView? = null
    var forum: ImageView? = null
    var reminders: ImageView? = null
    var messages: ImageView? = null

    internal var firebaseConnection = FirebaseConnection()

    //Strings

    var wholeName: String = ""
    var userEmail: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patient_events)

        profile = findViewById(R.id.img_profile)
        find = findViewById(R.id.img_find)
        forum = findViewById(R.id.img_home)
        reminders = findViewById(R.id.img_reminders)
        messages = findViewById(R.id.img_messages)

        profile?.setOnClickListener {
            showProfile()
        }

        find?.setOnClickListener {
            showFind()
        }

        forum?.setOnClickListener {
            showForum()
        }

        reminders?.setOnClickListener {
            showReminders()
        }

        leftMenu()
    }

    private fun leftMenu() {
        drawer {
            selectedItemByPosition = 9

            accountHeader {
                profile(wholeName, userEmail) {
                    icon = R.drawable.imgprofilepic
                }
                background = R.drawable.bgaccountheader
            }

            primaryItem("HEALTH TIPS") {
                icon = R.mipmap.ic_tipsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showHealthTips()
                    Log.d("DRAWER", "Health Tips Activity")
                    true
                }
            }

            primaryItem("EVENTS") {
                icon = R.mipmap.ic_eventsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showEvents()
                    Log.d("DRAWER", "Events Activity")
                    true
                }
            }

            primaryItem("ABOUT") {
                icon = R.mipmap.ic_helpicon
                textColorRes = R.color.White
                onClick { _ ->
                    showAbout()
                    Log.d("DRAWER", "About Activity/Fragment")
                    true
                }
            }

            primaryItem("MANAGE PRIVACY") {
                icon = R.mipmap.ic_settings
                textColorRes = R.color.White
                onClick { _ ->
                    showManagePrivacy()
                    Log.d("DRAWER", "Manage Privacy Activity")
                    true
                }
            }

            primaryItem("LOGOUT") {
                icon = R.mipmap.ic_logouticon
                textColorRes = R.color.White
                onClick { _ ->
                    logoutUser()
                    Log.d("DRAWER", "User Logout")
                    true
                }
            }

            sliderBackground = R.drawable.bgslider
        }
    }

    private fun logoutUser() {
        firebaseConnection.firebaseAuth.signOut()
        val intent = Intent(this@PatientEventsActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    private fun showAbout() {
        val intent = Intent(this@PatientEventsActivity, PatientAboutActivity::class.java)
        startActivity(intent)
    }

    private fun showManagePrivacy() {
        val intent = Intent(this@PatientEventsActivity, ManagePrivacyActivity::class.java)
        startActivity(intent)
    }

    private fun showEvents() {
        val intent = Intent(this@PatientEventsActivity, PatientEventsActivity::class.java)
        startActivity(intent)
    }

    private fun showHealthTips() {
        val intent = Intent(this@PatientEventsActivity, PatientHealthTipsActivity::class.java)
        startActivity(intent)
    }

    private fun showProfile() {
        val intent = Intent(this@PatientEventsActivity, ProfileActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showFind() {
        val intent = Intent(this@PatientEventsActivity, FindActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showForum() {
        val intent = Intent(this@PatientEventsActivity, ForumActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showReminders() {
        val intent = Intent(this@PatientEventsActivity, MedRemindersActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        intent.putExtras(extras)
        startActivity(intent)
    }
}
