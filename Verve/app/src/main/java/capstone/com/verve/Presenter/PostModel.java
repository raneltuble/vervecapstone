package capstone.com.verve.Presenter;

public class PostModel {
    String firstname, middlename, lastname, datePost, timePost, postDescription, postTitle, postCateg, uid, photoUrl;

    public PostModel() {
    }

    public PostModel(String firstname, String middlename, String lastname, String datePost,
                     String timePost, String postDescription, String postTitle,
                     String postCateg, String uid, String photoUrl) {
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
        this.datePost = datePost;
        this.timePost = timePost;
        this.postDescription = postDescription;
        this.postTitle = postTitle;
        this.postCateg = postCateg;
        this.uid = uid;
        this.photoUrl = photoUrl;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDatePost() {
        return datePost;
    }

    public void setDatePost(String datePost) {
        this.datePost = datePost;
    }

    public String getTimePost() {
        return timePost;
    }

    public void setTimePost(String timePost) {
        this.timePost = timePost;
    }

    public String getPostDescription() {
        return postDescription;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostCateg() {
        return postCateg;
    }

    public void setPostCateg(String postCateg) {
        this.postCateg = postCateg;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
