package capstone.com.verve.View.PatientActivities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.TabLayout
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.ManagePrivacy
import capstone.com.verve.Presenter.UserDetails
import capstone.com.verve.R
import capstone.com.verve.View.PatientAdapters.ProfilePagerAdapter
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.profile.profile
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_profile.*
import java.io.InputStream

class ProfileActivity : AppCompatActivity() {

    //Strings
    var wholeName: String = ""
    var userEmail: String = "";
    var userId: String = ""

    //ImageView & TexrViews
    var btn_edit: ImageView? = null
    var txt_name: TextView? = null
    var btn_settings: ImageView? = null
    var profile_img: ImageView? = null
    var txt_address: TextView? = null
    var txt_age: TextView? = null
    var txt_birthday: TextView? = null
    var txt_email: TextView? = null
    var txt_religion: TextView? = null
    var txt_username : TextView? = null
    var txt_gender: TextView? = null

    var tabs: TabLayout? = null
    var home: ImageView? = null
    var find: ImageView? = null
    var reminders: ImageView? = null
    val GALLERY_PICK:Int = 0
    internal var userDetails = UserDetails()
    internal var firebaseConnection = FirebaseConnection()
    var managePrivacy = ManagePrivacy()

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_profile)

        var extras = intent.extras

        wholeName = extras.getString("wholeName")
        userEmail = extras.getString("userEmail")
        userId = extras.getString("userId")

        profile_img = findViewById(R.id.profile_img)
        txt_username = findViewById(R.id.txt_username)
        txt_name = findViewById(R.id.txt_name)
        txt_address = findViewById(R.id.txt_address)
        txt_age = findViewById(R.id.txt_age)
        txt_birthday = findViewById(R.id.txt_birthday)
        txt_religion = findViewById(R.id.txt_religion)
        txt_email = findViewById(R.id.txt_email)
        txt_gender = findViewById(R.id.txt_gender)
        btn_edit = findViewById(R.id.btn_edit)
        btn_settings = findViewById(R.id.btn_settings)
        tabs = findViewById(R.id.tabLayout)
        home = findViewById(R.id.img_home)
        find = findViewById(R.id.img_find)
        reminders = findViewById(R.id.img_reminders)


        leftMenu()

        home?.setOnClickListener {
            showForum()
        }

        find?.setOnClickListener {
            showFind()
        }

        reminders?.setOnClickListener {
            showReminders()
        }

        profile_img?.setOnClickListener{
            var galleryIntent = Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            galleryIntent.type = "image/*"
            startActivityForResult(galleryIntent, GALLERY_PICK)
        }

        btn_settings?.setOnClickListener{
            val intent = Intent(this@ProfileActivity, ManagePrivacyActivity::class.java)
            var extras = Bundle()
            extras.putString("wholeName", wholeName)
            extras.putString("userEmail", userEmail)
            extras.putString("userId", userId)
            intent.putExtras(extras)
            startActivity(intent)
        }


        userDetails.getUserProfile(firebaseConnection.getOtherProfileReference("Users", userId ),
            txt_username, txt_name,
            txt_email, txt_birthday, txt_age, txt_address, txt_religion, txt_gender)


        tabs?.setTabTextColors(resources.getColor(R.color.LightGray), resources.getColor(R.color.White))
        for (i in 0 until tabs?.tabCount!!) {
            val tab = (tabs?.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(10, 0, 10, 0)
            tab.requestLayout()
        }

        val pageAdapter = ProfilePagerAdapter(supportFragmentManager, 3)
        viewpager.adapter = pageAdapter
        viewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
    }

    private fun showForum() {
        val intent = Intent(this@ProfileActivity, ForumActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showFind() {
        val intent = Intent(this@ProfileActivity, FindActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showReminders() {
        val intent = Intent(this@ProfileActivity, MedRemindersActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        var resultUri: Uri? = null

        if (requestCode == GALLERY_PICK && resultCode == Activity.RESULT_OK && data!=null){
            var imageUri = data.data

            CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .start(this)

            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){

                var result = CropImage.getActivityResult(data)

                if (requestCode == Activity.RESULT_OK){
                    resultUri = result.uri
                    Toast.makeText(this, resultUri.toString(), Toast.LENGTH_LONG).show()

                    profile_img!!.setImageURI(resultUri)
                }else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
                }

            }
        }
    }

    private fun leftMenu() {
        drawer {
            selectedItemByPosition = 9

            accountHeader {
                profile(wholeName, userEmail) {
                    icon = R.drawable.imgprofilepic
                }
                background = R.drawable.bgaccountheader
            }

            primaryItem("HEALTH TIPS") {
                icon = R.mipmap.ic_tipsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showHealthTips()
                    Log.d("DRAWER", "Health Tips Activity")
                    true
                }
            }

            primaryItem("EVENTS") {
                icon = R.mipmap.ic_eventsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showEvents()
                    Log.d("DRAWER", "Events Activity")
                    true
                }
            }

            primaryItem("ABOUT") {
                icon = R.mipmap.ic_helpicon
                textColorRes = R.color.White
                onClick { _ ->
                    showAbout()
                    Log.d("DRAWER", "About Activity/Fragment")
                    true
                }
            }

            primaryItem("MANAGE PRIVACY") {
                icon = R.mipmap.ic_settings
                textColorRes = R.color.White
                onClick { _ ->
                    showManagePrivacy()
                    Log.d("DRAWER", "Manage Privacy Activity")
                    true
                }
            }

            primaryItem("LOGOUT") {
                icon = R.mipmap.ic_logouticon
                textColorRes = R.color.White
                onClick { _ ->
                    logoutUser()
                    Log.d("DRAWER", "User Logout")
                    true
                }
            }

            sliderBackground = R.drawable.bgslider
        }
    }

    private fun logoutUser() {
        firebaseConnection.firebaseAuth.signOut()
        val intent = Intent(this@ProfileActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    private fun showAbout() {
        val intent = Intent(this@ProfileActivity, PatientAboutActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showManagePrivacy() {
        val intent = Intent(this@ProfileActivity, ManagePrivacyActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showEvents() {
        //val intent = Intent(this@ForumActivity, PatientEventsActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        val intent = Intent(this@ProfileActivity, PatientEventsActivity::class.java)
        startActivity(intent)
    }

    private fun showHealthTips() {
        val intent = Intent(this@ProfileActivity, PatientHealthTipsActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        //TODO: HIDE TABS
        if (!userId.equals(firebaseConnection.currentUser)) {
            managePrivacy.setVisible(txt_age, txt_address, txt_birthday, txt_email,
                txt_religion, txt_gender, btn_edit, btn_settings, userId)
        }else{

        }
    }
}
