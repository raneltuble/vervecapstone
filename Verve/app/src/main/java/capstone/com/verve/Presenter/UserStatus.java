package capstone.com.verve.Presenter;

import android.support.annotation.NonNull;
import android.widget.Toast;
import capstone.com.verve.API.FirebaseConnection;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public class UserStatus {
    FirebaseConnection firebaseConnection = new FirebaseConnection();

    public void updatePatientStatus(String status) {
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {

        } else {


            firebaseConnection.getSpecificNodeReference("Users").child(firebaseConnection.getCurrentUser()).child("status").setValue(status).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                    } else {
                    }
                }
            });
        }
    }

    public void updateDoctorStatus(String status) {
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {

        } else {
            firebaseConnection.getSpecificNodeReference("FindADoctor").child(firebaseConnection.getCurrentUser()).child("status").setValue(status).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                    } else {
                    }
                }
            });
        }


    }

    public void manageUserPresence() {
        firebaseConnection.userPresence().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected == true) {

                    updateDoctorStatus("Online");
                } else {
                    updateDoctorStatus("Offline");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
