package capstone.com.verve.Presenter;

public class MedicalRecord {
    String cancerType, cancerStage, treatment, bloodPressure, findings, diagnosis, prescriptions, doctorName, date, time, uid;

    public MedicalRecord() {
    }

//    public MedicalRecord(String cancerType, String cancerStage, String treatment, String bloodPressure, String findings, String diagnosis, String prescriptions) {
//        this.cancerType = cancerType;
//        this.cancerStage = cancerStage;
//        this.treatment = treatment;
//        this.bloodPressure = bloodPressure;
//        this.findings = findings;
//        this.diagnosis = diagnosis;
//        this.prescriptions = prescriptions;
//    }

    public MedicalRecord(String treatment, String bloodPressure, String findings, String diagnosis,
                         String prescriptions, String doctorName, String date, String time, String uid) {
        this.treatment = treatment;
        this.bloodPressure = bloodPressure;
        this.findings = findings;
        this.diagnosis = diagnosis;
        this.prescriptions = prescriptions;
        this.doctorName = doctorName;
        this.time = time;
        this.uid = uid;
        this.date = date;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCancerType() {
        return cancerType;
    }

    public void setCancerType(String cancerType) {
        this.cancerType = cancerType;
    }

    public String getCancerStage() {
        return cancerStage;
    }

    public void setCancerStage(String cancerStage) {
        this.cancerStage = cancerStage;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getFindings() {
        return findings;
    }

    public void setFindings(String findings) {
        this.findings = findings;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(String prescriptions) {
        this.prescriptions = prescriptions;
    }
}
