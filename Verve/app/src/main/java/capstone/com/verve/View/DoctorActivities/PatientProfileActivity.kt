package capstone.com.verve.View.DoctorActivities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.UserDetails
import capstone.com.verve.Presenter.UserStatus
import capstone.com.verve.R
import capstone.com.verve.View.PatientActivities.LoginActivity
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.profile.profile
import org.jetbrains.anko.find

class PatientProfileActivity : AppCompatActivity() {
    var userEmail: String? = null
    var wholeName: String? = null
    var patientId: String? = null
    var txt_name: TextView? = null
    var txt_username: TextView? = null
    var txt_address: TextView? = null
    var txt_age: TextView? = null
    var txt_religion: TextView? = null
    var txt_birthday: TextView? = null
    var txt_email: TextView? = null
    var userDetails = UserDetails()
    var firebaseConnection = FirebaseConnection()
    var addButton: ImageView? = null
    var addMedicalRecord: ImageView? = null
    var addTreatmentRem: ImageView? = null
    var addMedicineRem: ImageView? = null
    var opaque: ImageView? = null
    var txt_gender: TextView? = null

    var userStatus = UserStatus()

    var btnBack: ImageView? = null
    var btnHome: ImageView? = null
    var btnProfile: ImageView? = null
    var btnForum: ImageView? = null
    var btnMessages: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patient_profile)

        btnForum = findViewById(R.id.img_forum)
        btnHome = findViewById(R.id.img_home)
        btnProfile = findViewById(R.id.img_profile)

        btnForum?.setOnClickListener {
            showForum()
        }

        btnHome?.setOnClickListener {
            showPatientList()
        }

        btnProfile?.setOnClickListener {
            showProfile()
        }

//        leftMenu()

        addButton = findViewById(R.id.btn_add)
        addMedicalRecord = findViewById(R.id.btn_addmedrecord)
        addTreatmentRem = findViewById(R.id.btn_addtreatrem)
        addMedicineRem = findViewById(R.id.btn_addmedrem)
        opaque = findViewById(R.id.bg_opaque)

        txt_username = findViewById(R.id.txt_username)
        txt_name = findViewById(R.id.txt_name)
        txt_address = findViewById(R.id.txt_address)
        txt_age = findViewById(R.id.txt_age)
        txt_birthday = findViewById(R.id.txt_birthday)
        txt_email = findViewById(R.id.txt_email)
        txt_religion = findViewById(R.id.txt_religion)
        txt_gender = findViewById(R.id.txt_gender)

        var extras = intent.extras

        patientId = extras.getString("patientId")
        wholeName = extras.getString("wholeName")
        userEmail = extras.getString("userEmail")

        //TODO: txt_gender
        userDetails.getUserProfile(firebaseConnection.getOtherProfileReference("Users", patientId), txt_username,
            txt_name, txt_email, txt_birthday, txt_age, txt_address, txt_religion, txt_gender)


        addButton?.setOnClickListener {
            addMedicalRecord?.isClickable = true
            addTreatmentRem?.isClickable = true
            addMedicineRem?.isClickable = true
            opaque?.isClickable = true
            addMedicalRecord?.visibility= View.VISIBLE
            addTreatmentRem?.visibility = View.VISIBLE
            addMedicineRem?.visibility = View.VISIBLE
            opaque?.visibility = View.VISIBLE
        }

        opaque?.setOnClickListener {
            addMedicalRecord?.isClickable = false
            addTreatmentRem?.isClickable = false
            addMedicineRem?.isClickable = false
            opaque?.isClickable = false
            addMedicalRecord?.visibility= View.INVISIBLE
            addTreatmentRem?.visibility = View.INVISIBLE
            addMedicineRem?.visibility = View.INVISIBLE
            opaque?.visibility = View.INVISIBLE
        }

        addMedicalRecord?.setOnClickListener {
            showAddMedicalRecord()
        }

        addTreatmentRem?.setOnClickListener {
            showAddTreatReminder()
        }

        addMedicineRem?.setOnClickListener {
            showAddMedReminder()
        }
    }

    override fun onResume() {
        super.onResume()
        userStatus.updateDoctorStatus("Online")

    }

    override fun onPause(){
        super.onPause()
        userStatus.updateDoctorStatus("Offline")
    }


    private fun showAddMedicalRecord() {
        val intent = Intent(this@PatientProfileActivity, AddAMedicalRecordActivity::class.java)
        var extras = Bundle()
        extras.putString("patientId", patientId)
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showAddMedReminder() {
        val intent = Intent(this@PatientProfileActivity, AddAMedReminderActivity::class.java)
        intent.putExtra("patientId", patientId)
        startActivity(intent)
    }

    private fun showAddTreatReminder() {
        val intent = Intent(this@PatientProfileActivity, AddATreatReminderActivity::class.java)
        var extras = Bundle()
        extras.putString("patientId", patientId)
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun leftMenu() {
        drawer {
            selectedItemByPosition = 5
            accountHeader {
                profile(wholeName!!, userEmail) {
                    icon = R.drawable.imgprofilepic
                }
                background = R.drawable.bgaccountheader
            }


            primaryItem("EVENTS") {
                icon = R.mipmap.ic_eventsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showEvents()
                    Log.d("DRAWER", "Events Activity")
                    true
                }
            }

            primaryItem("ABOUT") {
                icon = R.mipmap.ic_helpicon
                textColorRes = R.color.White
                onClick { _ ->
                    showAbout()
                    Log.d("DRAWER", "About Activity/Fragment")
                    true
                }
            }

            primaryItem("LOGOUT") {
                icon = R.mipmap.ic_logouticon
                textColorRes = R.color.White
                onClick { _ ->
                    logoutUser()
                    Log.d("DRAWER", "User Logout")
                    true
                }
            }

            sliderBackground = R.drawable.bgslider
        }
    }

    private fun logoutUser() {
        userStatus.updateDoctorStatus("Offline")
        firebaseConnection.userSignout()
        val intent = Intent(this@PatientProfileActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)


        return
    }

    private fun showAbout() {
        val intent = Intent(this@PatientProfileActivity, DoctorAboutActivity::class.java)
        startActivity(intent)
    }

    private fun showEvents() {
        val intent = Intent(this@PatientProfileActivity, DoctorEventsActivity::class.java)
        startActivity(intent)
    }

    private fun showProfile() {
        val intent = Intent(this@PatientProfileActivity, DoctorProfileActivity::class.java)
        startActivity(intent)
    }

    private fun showForum() {
        val intent = Intent(this@PatientProfileActivity, DoctorForumActivity::class.java)
        startActivity(intent)
    }

    private fun showPatientList() {
        val intent = Intent(this@PatientProfileActivity, PatientListActivity::class.java)
        startActivity(intent)
    }
}
