package capstone.com.verve.Presenter;

public class MedReminders {
    String medsName, medsDate, medsTime, medsFreq, medsDays;

    public MedReminders(){
    }

    public MedReminders(String medsName, String medsDate, String medsTime, String medsFreq, String medsDays) {
        this.medsName = medsName;
        this.medsDate = medsDate;
        this.medsTime = medsTime;
        this.medsFreq = medsFreq;
        this.medsDays = medsDays;
    }

    public String getMedsName() {
        return medsName;
    }

    public void setMedsName(String medsName) {
        this.medsName = medsName;
    }

    public String getMedsDate() {
        return medsDate;
    }

    public void setMedsDate(String medsDate) {
        this.medsDate = medsDate;
    }

    public String getMedsTime() {
        return medsTime;
    }

    public void setMedsTime(String medsTime) {
        this.medsTime = medsTime;
    }

    public String getMedsFreq() {
        return medsFreq;
    }

    public void setMedsFreq(String medsFreq) {
        this.medsFreq = medsFreq;
    }

    public String getMedsDays() {
        return medsDays;
    }

    public void setMedsDays(String medsDays) {
        this.medsDays = medsDays;
    }
}
