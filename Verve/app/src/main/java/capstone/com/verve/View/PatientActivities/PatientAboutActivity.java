package capstone.com.verve.View.PatientActivities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import capstone.com.verve.R;
import com.codemybrainsout.onboarder.AhoyOnboarderActivity;
import com.codemybrainsout.onboarder.AhoyOnboarderCard;

import java.util.ArrayList;
import java.util.List;

public class PatientAboutActivity extends AhoyOnboarderActivity {

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        AhoyOnboarderCard aboutForum = new AhoyOnboarderCard("Health Forum", "Description", R.mipmap.ic_forumgreen);
        AhoyOnboarderCard aboutProfile = new AhoyOnboarderCard("Profile", "Description", R.mipmap.ic_profilegreen);
        AhoyOnboarderCard aboutFindDoctor = new AhoyOnboarderCard("Find a Doctor", "Description", R.mipmap.ic_find_green);
        AhoyOnboarderCard aboutMedReminder = new AhoyOnboarderCard("Medicine Reminders", "Description", R.mipmap.ic_medicinegreen);
        AhoyOnboarderCard aboutTreatReminder = new AhoyOnboarderCard("Treatment Reminders", "Description", R.mipmap.ic_medicinegreen);
        AhoyOnboarderCard aboutMessages = new AhoyOnboarderCard("Direct Messages", "Description", R.mipmap.ic_messagegreen);
        AhoyOnboarderCard aboutEvents = new AhoyOnboarderCard("Events", "Description", R.mipmap.ic_eventsicon);
        AhoyOnboarderCard aboutHealthTips = new AhoyOnboarderCard("Health Tips", "Description", R.mipmap.ic_tipsicon);

        aboutForum.setBackgroundColor(R.color.black_transparent);
        aboutProfile.setBackgroundColor(R.color.black_transparent);
        aboutFindDoctor.setBackgroundColor(R.color.black_transparent);
        aboutMedReminder.setBackgroundColor(R.color.black_transparent);
        aboutTreatReminder.setBackgroundColor(R.color.black_transparent);
        aboutMessages.setBackgroundColor(R.color.black_transparent);
        aboutEvents.setBackgroundColor(R.color.black_transparent);
        aboutHealthTips.setBackgroundColor(R.color.black_transparent);

        List<AhoyOnboarderCard> pages = new ArrayList<>();
        pages.add(aboutForum);
        pages.add(aboutProfile);
        pages.add(aboutFindDoctor);
        pages.add(aboutMedReminder);
        pages.add(aboutTreatReminder);
        pages.add(aboutMessages);
        pages.add(aboutEvents);
        pages.add(aboutHealthTips);

        for (AhoyOnboarderCard page: pages) {
            page.setTitleColor(R.color.white);
            page.setDescriptionColor(R.color.white);
            page.setTitleTextSize(dpToPixels(7, this));
            page.setDescriptionTextSize(dpToPixels(5, this));
            page.setIconLayoutParams(300, 300, 150, 20, 20, 0);
        }

        setFinishButtonTitle("Finish");
        showNavigationControls(true);
        setGradientBackground();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setFinishButtonDrawableStyle(ContextCompat.getDrawable(this, R.drawable.rounded_button));
        }

        setOnboardPages(pages);
    }

    @Override
    public void onFinishButtonPressed() {
        Activity activity = new PatientAboutActivity();

        if (activity != null) {
            activity.onBackPressed();
        }
    }
}
