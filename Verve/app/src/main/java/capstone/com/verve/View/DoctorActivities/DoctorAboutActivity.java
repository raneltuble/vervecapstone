package capstone.com.verve.View.DoctorActivities;

import android.app.Activity;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import capstone.com.verve.R;
import com.codemybrainsout.onboarder.AhoyOnboarderActivity;
import com.codemybrainsout.onboarder.AhoyOnboarderCard;

import java.util.ArrayList;
import java.util.List;

public class DoctorAboutActivity extends AhoyOnboarderActivity {

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AhoyOnboarderCard aboutPatientList = new AhoyOnboarderCard("Patient List", "Description", R.mipmap.ic_patient);
        AhoyOnboarderCard aboutForum = new AhoyOnboarderCard("Health Forum", "Description", R.mipmap.ic_forumgreen);
        AhoyOnboarderCard aboutProfile = new AhoyOnboarderCard("Profile", "Description", R.mipmap.ic_profilegreen);
        AhoyOnboarderCard aboutAddMedRec = new AhoyOnboarderCard("Add Medical Records", "Description", R.mipmap.ic_medicinegreen);
        AhoyOnboarderCard aboutAddMedRem = new AhoyOnboarderCard("Add Medicine Reminders", "Description", R.mipmap.ic_medicinegreen);
        AhoyOnboarderCard aboutAddTreatRem = new AhoyOnboarderCard("Add Treatment Reminders", "Description", R.mipmap.ic_medicinegreen);
        AhoyOnboarderCard aboutMessages = new AhoyOnboarderCard("Direct Messages", "Description", R.mipmap.ic_messagegreen);
        AhoyOnboarderCard aboutEvents = new AhoyOnboarderCard("Events", "Description", R.mipmap.ic_eventsicon);

        aboutForum.setBackgroundColor(R.color.black_transparent);
        aboutProfile.setBackgroundColor(R.color.black_transparent);
        aboutAddMedRec.setBackgroundColor(R.color.black_transparent);
        aboutAddMedRem.setBackgroundColor(R.color.black_transparent);
        aboutAddTreatRem.setBackgroundColor(R.color.black_transparent);
        aboutMessages.setBackgroundColor(R.color.black_transparent);
        aboutEvents.setBackgroundColor(R.color.black_transparent);
        aboutPatientList.setBackgroundColor(R.color.black_transparent);

        List<AhoyOnboarderCard> pages = new ArrayList<>();
        pages.add(aboutForum);
        pages.add(aboutProfile);
        pages.add(aboutAddMedRec);
        pages.add(aboutAddMedRem);
        pages.add(aboutAddTreatRem);
        pages.add(aboutMessages);
        pages.add(aboutEvents);
        pages.add(aboutPatientList);

        for (AhoyOnboarderCard page: pages) {
            page.setTitleColor(R.color.white);
            page.setDescriptionColor(R.color.white);
            page.setTitleTextSize(dpToPixels(6, this));
            page.setDescriptionTextSize(dpToPixels(5, this));
            page.setIconLayoutParams(300, 300, 150, 20, 20, 0);
        }

        setFinishButtonTitle("Finish");
        showNavigationControls(true);
        setGradientBackground();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setFinishButtonDrawableStyle(ContextCompat.getDrawable(this, R.drawable.rounded_button));
        }

        setOnboardPages(pages);
    }

    @Override
    public void onFinishButtonPressed() {
        Activity activity = new DoctorAboutActivity();

        if (activity != null) {
            activity.onBackPressed();
        }
    }
}