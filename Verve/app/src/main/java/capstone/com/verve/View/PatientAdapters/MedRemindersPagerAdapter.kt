package capstone.com.verve.View.PatientAdapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import capstone.com.verve.View.PatientFragments.MedsRemindersFragment
import capstone.com.verve.View.PatientFragments.TreatmentsRemindersFragment

class MedRemindersPagerAdapter (fm: FragmentManager?, numOfTabs: Int) : FragmentPagerAdapter(fm) {

    private var numOfTabs: Int = numOfTabs
    private var medsFragment = MedsRemindersFragment()
    private var treatmentsFragment = TreatmentsRemindersFragment()

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return medsFragment
            1 -> return treatmentsFragment
            else -> return null
        }
    }

    override fun getCount(): Int {
        return numOfTabs
    }
}