package capstone.com.verve.View.PatientActivities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.Registration
import capstone.com.verve.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import android.app.DatePickerDialog
import android.widget.*
import java.text.SimpleDateFormat
import java.util.*


class RegisterPatientActivity : AppCompatActivity() {

    //String
    var firstname: String? = null
    var middlename: String? = null
    var lastname: String? = null
    var username: String? = null
    var age: String? = null
    var mobilenum: String? = null
    var userEmail: String? = null
    var address: String? = null
    var birthdate: String? = null
    var gender: String? = null
    var religion: String? = null


    private var auth: FirebaseAuth? = null
    internal var register: Registration? = null

    var etxt_firstname: EditText? = null
    var etxt_middlename: EditText? = null
    var etxt_lastname: EditText? = null
    var etxt_username: EditText? = null
    var etxt_password: EditText? = null
    var etxt_mobile: EditText? = null
    var etxt_email: EditText? = null
    var etxt_address: EditText? = null
    var etxt_birthdate: EditText? = null
    var et_religion: EditText? = null
    var rad_male: RadioButton? = null
    var rad_female: RadioButton? = null
    var btn_datepicker: ImageButton? = null
    var btn_next: ImageView? = null
    private var user: FirebaseUser? = null
    internal var firebaseConnection = FirebaseConnection()
    private val myCalendar = Calendar.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_register_patient)
        register = Registration()
        etxt_firstname = findViewById(R.id.et_firstname)
        etxt_middlename = findViewById(R.id.et_middlename)
        etxt_lastname = findViewById(R.id.et_lastname)
        etxt_username = findViewById(R.id.et_username)
        etxt_mobile = findViewById(R.id.et_password)
        etxt_email = findViewById(R.id.et_email)
        etxt_address = findViewById(R.id.et_address)
        etxt_birthdate = findViewById(R.id.et_birthday)
        et_religion = findViewById(R.id.et_religion)
        btn_datepicker = findViewById(R.id.btn_datepicker)
        btn_next = findViewById(R.id.btn_next)

        rad_male = findViewById(R.id.radioMale);
        rad_female = findViewById(R.id.radioFemale)

        auth = firebaseConnection.firebaseAuth
        user = firebaseConnection.firebaseUser


        btn_datepicker?.setOnClickListener {
            DatePickerDialog(
                this@RegisterPatientActivity, datePickerListener, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        btn_next?.setOnClickListener {
            nextRegister()
        }
    }

    val datePickerListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(
            view: DatePicker, year: Int, monthOfYear: Int,
            dayOfMonth: Int
        ) {

            var today = Calendar.getInstance()
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val myFormat = "MM/dd/yy"
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            etxt_birthdate?.setText(sdf.format(myCalendar.time))
            age = Integer.toString(register!!.calculateAge(myCalendar.timeInMillis))
        }
    }


    fun option(v: View) {
        var i: Intent? = null
        val chooser: Intent? = null

        if (v.id == R.id.click_login) {
            i = Intent(this, LoginActivity::class.java)
            startActivity(i)
        }
    }

    private fun nextRegister() {
        val intent = Intent(this@RegisterPatientActivity, Register2PatientActivity::class.java)
        var extras = Bundle()
        firstname = etxt_firstname?.text.toString()
        middlename = etxt_middlename?.text.toString()
        lastname = etxt_lastname?.text.toString()
        username = etxt_username?.text.toString()
        userEmail = etxt_email?.text.toString()
        mobilenum = etxt_mobile?.text.toString()
        address = etxt_address?.text.toString()
        birthdate = etxt_birthdate?.text.toString()
        religion = et_religion?.text.toString()

        if (rad_male!!.isChecked) {
            gender = "Male"
        } else if (rad_female!!.isChecked) {
            gender = "Female"
        }

        if (firstname!!.isEmpty()) {
            etxt_firstname!!.error = "First Name is Required!"
            etxt_firstname!!.requestFocus()
            return
        }
        if (lastname!!.isEmpty()) {
            etxt_lastname!!.error = "Last Name is Required!"
            etxt_lastname!!.requestFocus()
            return
        }
        if (username!!.isEmpty()) {
            etxt_username!!.error = "Username is Required!"
            etxt_username!!.requestFocus()
            return
        }
        if (userEmail!!.isEmpty()) {
            etxt_email!!.error = "Email is Required!"
            etxt_email!!.requestFocus()
            return
        }
        if (mobilenum!!.isEmpty() && mobilenum!!.length < 11) {
            etxt_mobile!!.error = "Mobile Number is empty or in wrong format!"
            etxt_mobile!!.requestFocus()

            return
        }
        if (address!!.isEmpty()) {
            etxt_address!!.error = "Address is required!"
            etxt_address!!.requestFocus()
            return
        }

        if (birthdate!!.isEmpty()) {
            etxt_birthdate!!.error = "Birthdate is required!"
            etxt_birthdate!!.requestFocus()
            return
        }
        if (religion!!.isEmpty()) {
            et_religion!!.error = "Religion is required!"
            et_religion!!.requestFocus()
            return
        }
        if(!rad_male!!.isChecked && !rad_female!!.isChecked){
            Toast.makeText(this, "Please Select a Gender", Toast.LENGTH_LONG).show()
            return
        }

        extras.putString("firstname", firstname)
        extras.putString("middlename", middlename)
        extras.putString("lastname", lastname)
        if (!middlename!!.isEmpty()) {
            extras.putString("middlename", middlename)
        } else {
            extras.putString("middlename", "")
        }
        extras.putString("username", username)
        extras.putString("userEmail", userEmail)
        extras.putString("mobilenum", mobilenum)
        extras.putString("address", address)
        extras.putString("birthdate", birthdate)
        extras.putString("religion", religion)
        extras.putString("age", age)
        extras.putString("gender", gender)
        intent.putExtras(extras)
        startActivity(intent)
    }
}
