package capstone.com.verve.View.PatientFragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import capstone.com.verve.API.FirebaseConnection;
import capstone.com.verve.Interface.AcceptListener;
import capstone.com.verve.Presenter.Posts;
import capstone.com.verve.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class ForumAddPostFrag extends DialogFragment implements AcceptListener {

    //Objects
    FirebaseConnection firebaseConnection = new FirebaseConnection();
    Posts posts = new Posts();

    //DatabaseReference
    DatabaseReference userRef;
    DatabaseReference postRef;
    DatabaseReference postLog;
    FirebaseAuth auth;

    //RES
    EditText postTitle, descTitle;
    Button acceptButton, cancelButton;
    Spinner spinner;
    ImageView btn_addimage;

    //STRING
    String saveCurrentDate;
    String saveCurrentTime;
    String postRandomName;

    Uri imageUri = null;

    int GALLERY_PICK = 0;

    OnFragmentInteractionListener listener = null;
    AcceptListener accept;

    public ForumAddPostFrag() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_post, container, false);

        postTitle = rootView.findViewById(R.id.txt_your_name);
        descTitle = rootView.findViewById(R.id.txt_your_num);
        acceptButton = rootView.findViewById(R.id.buttonAccept);
        cancelButton = rootView.findViewById(R.id.buttonCancel);
        spinner = rootView.findViewById(R.id.spinner);
        btn_addimage = rootView.findViewById(R.id.btn_addimage);

        userRef = firebaseConnection.getSpecificNodeReference("Users");
        postRef = firebaseConnection.getSpecificNodeReference("Posts");
        postLog = firebaseConnection.getSpecificNodeReference("PostLogs");
        auth = firebaseConnection.getFirebaseAuth();

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FragmentActivity activity = getActivity();
                if (imageUri != null) {

                    StorageReference filePath = firebaseConnection.storageReference().child("Post Image")
                            .child(imageUri.getLastPathSegment().concat(".jpg"));
                    filePath.putFile(imageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                            //Toast.makeText(getActivity(), "Image is sucessfully stored in the database...", Toast.LENGTH_SHORT).show();
                            Task<Uri> result = task.getResult().getMetadata().getReference().getDownloadUrl();

                            result.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    final String downloadUrl = uri.toString();
                                    posts.savePosts(userRef, postRef, postLog, auth, postTitle,
                                            descTitle, spinner, activity, downloadUrl);
                                    //Toast.makeText(activity, "Post Added Successfully", Toast.LENGTH_LONG ).show();
                                }
                            });

                        }
                    });
                } else {
                    posts.savePosts(userRef, postRef, postLog, auth, postTitle,
                            descTitle, spinner, activity,"");
                }

                dismiss();
            }

        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btn_addimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        return rootView;
    }


    public void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, GALLERY_PICK);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_PICK && resultCode == Activity.RESULT_OK && data != null) {
            imageUri = data.getData();
        }
    }

    @Override
    public void onSubmit() {

    }

    @Override
    public void onResume() {

        super.onResume();

    }

    public void setListener(AcceptListener acceptListener) {
        accept = acceptListener;
    }

    interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


    public static ForumAddPostFrag newInstance(String title) {
        ForumAddPostFrag frag = new ForumAddPostFrag();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);

        return frag;
    }


}
