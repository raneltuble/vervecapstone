package capstone.com.verve.View.DoctorFragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import capstone.com.verve.API.FirebaseConnection;
import capstone.com.verve.Presenter.PostModel;
import capstone.com.verve.R;
import capstone.com.verve.View.DoctorActivities.DoctorProfileActivity;
import capstone.com.verve.View.PatientActivities.ForumViewPostCommentsActivity;
import capstone.com.verve.View.PatientActivities.ProfileActivity;
import capstone.com.verve.View.PatientFragments.ForumFragment;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.*;
import com.squareup.picasso.Picasso;

public class DoctorPostsFrag extends Fragment {

    RecyclerView mRecyclerView;

    public FirebaseRecyclerAdapter<PostModel, ForumFragment.PostViewHolder> firebaseRecyclerAdapter;

    Query query;

    TextView txtName;
    TextView txtHearts;
    TextView txtComments;

    ImageView imgComment;
    ImageView img_heart;
    ImageView postImageView;

    DatabaseReference likesRef;
    DatabaseReference userRef;

    int countLikes;
    int countComments;

    String wholeName;
    String userEmail;
    String userId;


    Boolean likeChecker;


    FirebaseConnection firebaseConnection = new FirebaseConnection();

    String currentUserId = firebaseConnection.getCurrentUser();

    DatabaseReference postRef = firebaseConnection.getSpecificNodeReference("Posts");

    DatabaseReference commentRef;

    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        DoctorProfileActivity activity = (DoctorProfileActivity)getActivity();

        wholeName = activity.getWholeName();
        userEmail = activity.getUserEmail();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View rootView = inflater.inflate(R.layout.fragment_profile_posts, container, false);
        mRecyclerView = rootView.findViewById(R.id.postsRecyclerView);
        mRecyclerView.hasFixedSize();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(layoutManager);

        return rootView;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

    }

    public static class PostViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public PostViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setDetails(String firstname, String middlename,
                               String lastname, String postDescription,
                               String postTitle, String datePost,
                               String timePost, String postCateg,String photoUrl){

            //NAME
            TextView txtName = (TextView) mView.findViewById(R.id.txtName);
            String noMiddlename = "";
            String withMiddleName = "";

            if (middlename.isEmpty()) {
                noMiddlename = firstname.concat(" ".concat(lastname));
                txtName.setText(noMiddlename);
            } else {
                withMiddleName = firstname.concat(" ".concat(middlename.substring(0, 1)) + ". ".concat(lastname));
                txtName.setText(withMiddleName);
            }

            //POST DESCRIPTION
            TextView txtPostDetails = (TextView) mView.findViewById(R.id.txtPostDetails);
            txtPostDetails.setText(postDescription);

            //POST TITLE
            TextView txtPostTitle = mView.findViewById(R.id.txtPostTitle);
            txtPostTitle.setText(postTitle);

            //DATE POST
            TextView txtDate = mView.findViewById(R.id.txtDate);
            txtDate.setText(datePost);

            //TIME
            TextView txtTime = mView.findViewById(R.id.txtTime);
            txtTime.setText(timePost);

            //POST CATEGORY
            TextView txtCancerType = mView.findViewById(R.id.txtCancerType);
            txtCancerType.setText(postCateg);

            //IMAGE
            ImageView postImageView = mView.findViewById(R.id.postImageView);
            if (!photoUrl.isEmpty()) {
                Picasso.get().load(photoUrl).placeholder(R.drawable.bg_blur_hospital).into(postImageView);
            } else {
                postImageView.setVisibility(ImageView.GONE);
            }
        }
    }


    public void setCommentCount(String key, final TextView txtComments) {
        postRef.child(key).child("Comments").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    countComments = (int) dataSnapshot.getChildrenCount();
                    txtComments.setText(Integer.toString(countComments));
                } else {
                    txtComments.setText("0");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    @Override
    public void onStart() {

        super.onStart();

        postRef = firebaseConnection.getSpecificNodeReference("Posts");
        currentUserId = firebaseConnection.getCurrentUser();

//        if (currentUserId == userId){
            query = postRef.orderByChild("uid").startAt(currentUserId).endAt(currentUserId + "\uf8ff");
//        }else{
//            query = postRef.orderByChild("uid").startAt(userId).endAt(userId + "\uf8ff");
//        }

        FirebaseRecyclerOptions<PostModel> options = new FirebaseRecyclerOptions.Builder<PostModel>()
                .setQuery(query, PostModel.class).build();
        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<PostModel, ForumFragment.PostViewHolder>(options) {
            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public int getItemViewType(int position) {
                return position;
            }


            @Override
            protected void onBindViewHolder(@NonNull ForumFragment.PostViewHolder holder, int position, @NonNull PostModel model) {

                final String key = getRef(position).getKey();
                //setLikeButtonStatus(key);
                setCommentCount(key, txtComments);
                holder.setDetails(model.getFirstname(), model.getMiddlename(),
                        model.getLastname(), model.getPostDescription(),
                        model.getPostTitle(), model.getDatePost(),
                        model.getTimePost(), model.getPostCateg(),model.getPhotoUrl());

                txtName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        postRef.child(key).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String userId = dataSnapshot.child("uid").getValue(String.class);
                                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                                Bundle extras = new Bundle();
                                extras.putString("wholeName", wholeName);
                                extras.putString("userEmail", userEmail);
                                extras.putString("userId", userId);
                                intent.putExtras(extras);
                                startActivity(intent);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                });

                imgComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getActivity(), ForumViewPostCommentsActivity.class);
                        Bundle extras = new Bundle();
                        extras.putString("wholeName", wholeName);
                        extras.putString("userEmail", userEmail);
                        extras.putString("postId", key);
                        intent.putExtras(extras);
                        startActivity(intent);
                    }
                });

                img_heart.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        likeChecker = true;
                        likesRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                if (likeChecker.equals(false)) {
                                    if (dataSnapshot.child(key).hasChild(currentUserId)) {
                                        likesRef.child(key).child(currentUserId).removeValue();
                                        likeChecker = false;
                                    }
                                } else {
                                    likesRef.child(key).child(currentUserId).setValue(true);
                                    likeChecker = true;
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                });

            }

            @NonNull
            @Override
            public ForumFragment.PostViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                final View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.item_post_forum, viewGroup, false);

                txtName = view.findViewById(R.id.txtName);
                imgComment = view.findViewById(R.id.img_comment);
                img_heart = view.findViewById(R.id.img_heart);
                txtHearts = view.findViewById(R.id.txtHearts);
                txtComments = view.findViewById(R.id.txtComments);
                postImageView = view.findViewById(R.id.postImageView);

                return new ForumFragment.PostViewHolder(view);
            }
        };

        firebaseRecyclerAdapter.setHasStableIds(true);
        mRecyclerView.setAdapter(firebaseRecyclerAdapter);
        firebaseRecyclerAdapter.startListening();

    }


}
