package capstone.com.verve.Presenter;

public class TreatmentReminders {
    String treatmentName, date, time, doctorName, uid;

    public TreatmentReminders() {
    }

    public TreatmentReminders(String treatmentName, String date, String time, String doctorName, String uid) {
        this.treatmentName = treatmentName;
        this.date = date;
        this.time = time;
        this.doctorName = doctorName;
        this.uid = uid;
    }

    public String getTreatmentName() {
        return treatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        this.treatmentName = treatmentName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
