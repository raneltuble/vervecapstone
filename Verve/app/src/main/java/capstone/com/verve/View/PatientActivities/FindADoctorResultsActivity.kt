package capstone.com.verve.View.PatientActivities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import capstone.com.verve.API.Database
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.DoctorList
import capstone.com.verve.Presenter.FindADoctor
import capstone.com.verve.Presenter.PatientList
import capstone.com.verve.Presenter.UserComments
import capstone.com.verve.R
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.item_findadoctor.view.*

class FindADoctorResultsActivity : AppCompatActivity() {

    var doctorName: TextView? = null
    var doctorSpecialty: TextView? = null
    var btnConsult: ImageView? = null
    var btnProfile: ImageView? = null
    var findADoctorId: String? = ""
    internal var firebaseConnection = FirebaseConnection()
    private var recyclerView: RecyclerView? = null
    lateinit var dDatabase: DatabaseReference

    //Current User Id

    var currUser: String? = null

    //Database References
    var doctorsListPref: DatabaseReference? = null
    var patientListPref: DatabaseReference? = null
    var findADoctorPref: DatabaseReference? = null
    var patientPref: DatabaseReference? = null

    //Doctor's Info
    var doctorFirstname: String? = null
    var doctorMiddlename: String? = null
    var doctorLastname: String? = null
    var doctorAreaOfExcellence: String? = null
    var doctorUid: String? = null

    //Patient's Info
    var patientFirstname: String? = null
    var patientMiddlename: String? = null
    var patientLastName: String? = null
    var patientTreatment: String? = ""
    var patientCancerType: String? = ""
    var patientUid: String? = null


    //Intents
    var wholeName: String? = ""
    var userEmail: String? = ""
    var userId: String? = ""

    var imgForum: ImageView? = null
    var imgProfile: ImageView? = null
    var imgReminders: ImageView? = null
    var imgChat: ImageView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_find_adoctor_results)

        recyclerView = findViewById(R.id.rv_doctorslist)
        var extras = intent.extras

        wholeName = extras.getString("wholeName")
        userEmail = extras.getString("userEmail")
        userId = extras.getString("userId")

        imgReminders = findViewById(R.id.img_reminders)
        imgForum = findViewById(R.id.img_home)
        imgProfile = findViewById(R.id.img_profile)
        imgChat = findViewById(R.id.img_messages)

        imgForum?.setOnClickListener {
            showForum()
        }

        imgProfile?.setOnClickListener {
            showProfile()
        }

        imgReminders?.setOnClickListener {
            showReminders()
        }

        DoctorsRecyclerView()
    }

    private fun showForum() {
        val intent = Intent(this@FindADoctorResultsActivity, ForumActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showProfile() {
        val intent = Intent(this@FindADoctorResultsActivity, ProfileActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showReminders() {
        val intent = Intent(this@FindADoctorResultsActivity, MedRemindersActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    fun DoctorsRecyclerView() {
        dDatabase = FirebaseDatabase.getInstance().reference
        val doctorListQuery = dDatabase
            .child("FindADoctor")
            .orderByChild("status")
            .equalTo("Online")

        //TODO: DOCTOR MODEL
        val doctorListOptions = FirebaseRecyclerOptions.Builder<FindADoctor>()
            .setQuery(doctorListQuery, FindADoctor::class.java)
            .setLifecycleOwner(this)
            .build()

        var layoutManager = LinearLayoutManager(this@FindADoctorResultsActivity)
        recyclerView?.layoutManager = layoutManager

        var mDoctorListViewHolder = object : FirebaseRecyclerAdapter<FindADoctor, DoctorListViewHolder>(doctorListOptions) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoctorListViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_findadoctor, parent, false)

                doctorName = view.findViewById(R.id.txt_doctorname)
                doctorSpecialty = view.findViewById(R.id.txt_doctorspecialty)
                btnConsult = view.findViewById(R.id.btn_consultnow)
                btnProfile = view.findViewById(R.id.btn_viewprofile)

                doctorsListPref = firebaseConnection.getSpecificNodeReference("DoctorList")
                patientListPref = firebaseConnection.getSpecificNodeReference("PatientList")
                findADoctorPref = firebaseConnection.getSpecificNodeReference("FindADoctor")
                patientPref = firebaseConnection.getSpecificNodeReference("Users")

                currUser = firebaseConnection.currentUser

                return DoctorListViewHolder(view)
            }

            override fun onBindViewHolder(holder: DoctorListViewHolder, position: Int, model: FindADoctor) {
                holder.bind(model)

                btnConsult?.setOnClickListener {
                    findADoctorId = getRef(position).key!!

                    findADoctorPref!!.child(findADoctorId!!).addValueEventListener(object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                             doctorFirstname = dataSnapshot.child("firstname").getValue(String::class.java)
                             doctorMiddlename = dataSnapshot.child("middlename").getValue(String::class.java)
                             doctorLastname = dataSnapshot.child("lastname").getValue(String::class.java)
                             doctorAreaOfExcellence = dataSnapshot.child("specialty").getValue(String::class.java)
                             doctorUid = dataSnapshot.child("uid").getValue(String::class.java)


                            //Traverse to List of Doctors
                            doctorsListPref!!.child(currUser!!).addValueEventListener(object: ValueEventListener {
                                override fun onDataChange(ds: DataSnapshot) {
                                    //Check if doctor is existing in the list
                                    if (ds.hasChild(doctorUid!!)){

                                        //Go to Messages

                                    }else{
                                        val doctorList = DoctorList(
                                            doctorFirstname, doctorMiddlename,
                                            doctorLastname, doctorAreaOfExcellence,
                                            doctorUid
                                        )

                                        doctorsListPref!!.child(currUser!!).child(doctorUid!!)
                                            .setValue(doctorList).addOnCompleteListener(
                                                OnCompleteListener<Void> { task ->
                                                    if (task.isSuccessful) {
                                                        Toast.makeText(this@FindADoctorResultsActivity,
                                                            "Doctor Added To Your List of Consulting Doctors",
                                                            Toast.LENGTH_SHORT).show()
                                                    } else {


                                                    }
                                                })
                                    }

                                }

                                override fun onCancelled(dataSnapshot: DatabaseError) {
                                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                                }
                            })

                            //END OF TRAVERSING IN ADDING TO DOCTORS LIST


                            //START TRAVERSING IN PATIENTS LIST
                            patientPref!!.child(currUser!!).addValueEventListener(object: ValueEventListener{
                                override fun onDataChange(dataSnapshot: DataSnapshot) {
                                    patientFirstname = dataSnapshot.child("firstname").getValue(String::class.java)
                                    patientMiddlename = dataSnapshot.child("middlename").getValue(String::class.java)
                                    patientLastName = dataSnapshot.child("lastname").getValue(String::class.java)

                                    patientListPref!!.child(doctorUid!!).addValueEventListener(object: ValueEventListener{
                                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                                             if (dataSnapshot.hasChild(currUser!!)){

                                             }else{
                                                 val patientList = PatientList(
                                                     patientFirstname, patientMiddlename,
                                                     patientLastName, patientTreatment, patientCancerType)

                                                 patientListPref!!.child(doctorUid!!).child(currUser!!)
                                                     .setValue(patientList).addOnCompleteListener(
                                                         OnCompleteListener<Void> { task ->
                                                             if (task.isSuccessful) {
                                                             } else {


                                                             }
                                                         })
                                             }
                                        }

                                        override fun onCancelled(dataSnapshot: DatabaseError) {
                                            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                                        }
                                    })

                                }

                                override fun onCancelled(databaseError: DatabaseError) {

                                }

                            })
                        }

                        override fun onCancelled(databaseError: DatabaseError) {

                        }
                    })
                }

                btnProfile?.setOnClickListener {
                    //TODO: Redirect to selected doctor's profile
                }
            }

        }

        recyclerView?.adapter = mDoctorListViewHolder
    }

    class DoctorListViewHolder(val cView: View, var doctors: FindADoctor? = null) : RecyclerView.ViewHolder(cView) {

        fun bind(doctors: FindADoctor) = with(doctors) {

            cView.txt_doctorname.text = doctors.firstname
            cView.txt_doctorspecialty.text = doctors.specialty
        }
    }
}
