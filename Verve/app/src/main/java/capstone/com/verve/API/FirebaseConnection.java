package capstone.com.verve.API;

import android.graphics.Bitmap;
import android.provider.MediaStore;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.InputStream;

public class FirebaseConnection {
    FirebaseDatabase firebaseDatabase;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    DatabaseReference databaseReference;
    DatabaseReference profileReference;
    DatabaseReference specificNodeReference;
    DatabaseReference connectedRef;
    String currentUserId = "";

    public FirebaseAuth getFirebaseAuth() {
        return firebaseAuth = FirebaseAuth.getInstance();
    }
    public void userSignout(){
        getFirebaseAuth().signOut();
    }

    public FirebaseUser getFirebaseUser() {
        return firebaseUser = getFirebaseAuth().getCurrentUser();
    }

    public String getCurrentUser() {
        return currentUserId = getFirebaseUser().getUid();
    }

    public FirebaseDatabase getFirebaseDatabase() {
        return firebaseDatabase = FirebaseDatabase.getInstance();
    }

    public DatabaseReference getFirebaseDatabaseReference() {
        return databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    public DatabaseReference getSpecificNodeReference(String node) {
        return specificNodeReference = getFirebaseDatabaseReference().child(node);
    }

    public DatabaseReference getProfileReference(String node) {
        return profileReference = getFirebaseDatabaseReference().child(node).child(getCurrentUser());
    }

    public DatabaseReference getOtherProfileReference(String node, String uid) {
        return profileReference = getFirebaseDatabaseReference().child(node).child(uid);
    }

    public DatabaseReference userPresence() {
        return connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
    }

    public StorageReference storageReference(){
        return FirebaseStorage.getInstance().getReference();
    }




}
