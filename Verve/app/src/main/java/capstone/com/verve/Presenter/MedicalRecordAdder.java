package capstone.com.verve.Presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;
import capstone.com.verve.API.FirebaseConnection;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class MedicalRecordAdder {
    FirebaseConnection firebaseConnection = new FirebaseConnection();


    public void addMedicalRecord(String uid, String treatment, String bp,
                                 String findings, String diagnosis, String prescriptions,
                                 String doctorName, String date, String time, String dateTodayRecord, String timeTodayRecord, final Context context){

        MedicalRecord medicalRecord = new MedicalRecord(treatment, bp, findings, diagnosis,
                prescriptions, doctorName, dateTodayRecord,timeTodayRecord, uid );

        firebaseConnection.getSpecificNodeReference("PatientList")
                .child(firebaseConnection.getCurrentUser()).child(uid)
                .child("treatment").setValue(treatment)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                } else {
                }
            }
        });
        firebaseConnection.getSpecificNodeReference("MedicalRecord").child(uid).child(uid.concat(date.concat(time))).setValue(medicalRecord).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(context, "Medical Record Added Successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
