package capstone.com.verve.Presenter;

public class FindADoctor {
    String firstname, middlename, lastname, specialty, status, userid;


    public FindADoctor() {
    }



    public FindADoctor(String firstname, String middlename, String lastname, String specialty, String status, String userid) {
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
        this.specialty = specialty;
        this.status = status;
        this.userid = userid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }



}
