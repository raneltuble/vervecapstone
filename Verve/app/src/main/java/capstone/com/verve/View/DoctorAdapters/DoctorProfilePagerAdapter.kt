package capstone.com.verve.View.DoctorAdapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import capstone.com.verve.View.DoctorFragments.DoctorAboutFragment
import capstone.com.verve.View.DoctorFragments.DoctorPostsFrag
import capstone.com.verve.View.DoctorFragments.DoctorPostsFragment
import capstone.com.verve.View.DoctorFragments.DoctorScheduleFragment

class DoctorProfilePagerAdapter (fm: FragmentManager?, numOfTabs: Int) : FragmentPagerAdapter(fm) {

    private var numOfTabs: Int = numOfTabs
    private var postsFragment = DoctorPostsFrag()
    private var aboutFragment = DoctorAboutFragment()
    private var scheduleFragment = DoctorScheduleFragment()


    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return postsFragment
            1 -> return aboutFragment
            2 -> return scheduleFragment
            else -> return null
        }    }

    override fun getCount(): Int {
        return numOfTabs
    }
}