package capstone.com.verve.View.DoctorActivities

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.Log
import android.widget.*
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.UserStatus
import capstone.com.verve.R
import capstone.com.verve.View.PatientActivities.LoginActivity
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.profile.profile
import java.text.SimpleDateFormat
import java.util.*

class AddAMedReminderActivity : AppCompatActivity() {

    var et_startdate: EditText? = null
    var time1: TextView? = null
    var time2: TextView? = null
    var time3: TextView? = null

    private val myCalendar = Calendar.getInstance()

    var btnBack: ImageView? = null
    var btnHome: ImageView? = null
    var btnProfile: ImageView? = null
    var btnForum: ImageView? = null
    var btnMessages: ImageView? = null

    var userEmail: String? = null
    var wholeName: String? = null
    internal var firebaseConnection = FirebaseConnection()
    var userStatus = UserStatus()

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_amed_reminder)

        val extras = intent.extras
        wholeName = extras.getString("wholeName")
        userEmail = extras.getString("userEmail")

        btnForum = findViewById(R.id.img_forum)
        btnHome = findViewById(R.id.img_home)
        btnProfile = findViewById(R.id.img_profile)

        btnForum?.setOnClickListener {
            showForum()
        }

        btnHome?.setOnClickListener {
            showPatientList()
        }

        btnProfile?.setOnClickListener {
            showProfile()
        }

        leftMenu()

        et_startdate = findViewById(R.id.et_startdate)
        time1 = findViewById(R.id.et_time1)
        time2 = findViewById(R.id.et_time2)
        time3 = findViewById(R.id.et_time3)

        et_startdate?.setOnClickListener {
            DatePickerDialog(
                this@AddAMedReminderActivity, datePickerListener, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        time1?.setOnClickListener {
            val c = Calendar.getInstance()
            val hour = c.get(Calendar.HOUR)
            val minute = c.get(Calendar.MINUTE)

            val tpd = TimePickerDialog(this,TimePickerDialog.OnTimeSetListener(function = { view, h, m ->
                var time = "$h:$m  "
                time1?.text = time
            }),hour,minute,false)
            tpd.show()
        }

        time2?.setOnClickListener {
            val c = Calendar.getInstance()
            val hour = c.get(Calendar.HOUR)
            val minute = c.get(Calendar.MINUTE)

            val tpd = TimePickerDialog(this,TimePickerDialog.OnTimeSetListener(function = { view, h, m ->
                var time = "$h:$m  "
                time2?.text = time
            }),hour,minute,false)
            tpd.show()
        }

        time3?.setOnClickListener {
            val c = Calendar.getInstance()
            val hour = c.get(Calendar.HOUR)
            val minute = c.get(Calendar.MINUTE)

            val tpd = TimePickerDialog(this,TimePickerDialog.OnTimeSetListener(function = { view, h, m ->
                var time = "$h:$m  "
                time3?.text = time
            }),hour,minute,false)
            tpd.show()
        }

    }

    var datePickerListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        var today = Calendar.getInstance()
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, monthOfYear)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        var myFormat = "MM/dd/yy"
        var sdf = SimpleDateFormat(myFormat, Locale.US)
        et_startdate?.setText(sdf.format(myCalendar.time))
    }

    override fun onResume() {
        super.onResume()
        userStatus.updateDoctorStatus("Online")

    }

    override fun onPause(){
        super.onPause()
        userStatus.updateDoctorStatus("Offline")
    }

    private fun leftMenu() {
        drawer {
            selectedItemByPosition = 5
            accountHeader {
                profile(wholeName!!, userEmail) {
                    icon = R.drawable.imgprofilepic
                }
                background = R.drawable.bgaccountheader
            }


            primaryItem("EVENTS") {
                icon = R.mipmap.ic_eventsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showEvents()
                    Log.d("DRAWER", "Events Activity")
                    true
                }
            }

            primaryItem("ABOUT") {
                icon = R.mipmap.ic_helpicon
                textColorRes = R.color.White
                onClick { _ ->
                    showAbout()
                    Log.d("DRAWER", "About Activity/Fragment")
                    true
                }
            }

            primaryItem("LOGOUT") {
                icon = R.mipmap.ic_logouticon
                textColorRes = R.color.White
                onClick { _ ->
                    logoutUser()
                    Log.d("DRAWER", "User Logout")
                    true
                }
            }

            sliderBackground = R.drawable.bgslider
        }
    }

    private fun logoutUser() {
        userStatus.updateDoctorStatus("Offline")
        firebaseConnection.userSignout()
        val intent = Intent(this@AddAMedReminderActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)


        return
    }

    private fun showAbout() {
        val intent = Intent(this@AddAMedReminderActivity, DoctorAboutActivity::class.java)
        startActivity(intent)
    }

    private fun showEvents() {
        val intent = Intent(this@AddAMedReminderActivity, DoctorEventsActivity::class.java)
        startActivity(intent)
    }

    private fun showProfile() {
        val intent = Intent(this@AddAMedReminderActivity, DoctorProfileActivity::class.java)
        startActivity(intent)
    }

    private fun showForum() {
        val intent = Intent(this@AddAMedReminderActivity, DoctorForumActivity::class.java)
        startActivity(intent)
    }

    private fun showPatientList() {
        val intent = Intent(this@AddAMedReminderActivity, PatientListActivity::class.java)
        startActivity(intent)
    }
}


