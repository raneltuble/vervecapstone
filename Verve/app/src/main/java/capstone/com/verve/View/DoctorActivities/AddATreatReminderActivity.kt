package capstone.com.verve.View.DoctorActivities

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.Log
import android.view.View
import android.widget.*
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.TreatmentReminderAdder
import capstone.com.verve.Presenter.UserStatus
import capstone.com.verve.R
import capstone.com.verve.View.PatientActivities.LoginActivity
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.profile.profile
import kotlinx.android.synthetic.main.activity_patient_profile.*
import java.text.SimpleDateFormat
import java.util.*

class AddATreatReminderActivity : AppCompatActivity() {
    //Strings
    var patientId: String? = null
    var treatmentName: String? = null
    var dateToday: String? = null
    var dateRecord: String? = null
    var timeToday: String?  = null
    var timeRecord: String? = null

    //Edit Text
    var et_treatmentname: EditText? = null
    var et_startdate: EditText? = null
    var tv_time: TextView? = null

    //Button
    var btn_addreminder: Button? = null
    var btn_addanothertreatment: Button? = null

    var treatmentReminderAdder = TreatmentReminderAdder()
    private val myCalendar = Calendar.getInstance()

    var btnBack: ImageView? = null
    var btnHome: ImageView? = null
    var btnProfile: ImageView? = null
    var btnForum: ImageView? = null
    var btnMessages: ImageView? = null

    var userEmail: String? = null
    var wholeName: String? = null
    internal var firebaseConnection = FirebaseConnection()
    var userStatus = UserStatus()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_atreat_reminder)

        btnForum = findViewById(R.id.img_forum)
        btnHome = findViewById(R.id.img_home)
        btnProfile = findViewById(R.id.img_profile)

        btnForum?.setOnClickListener {
            showForum()
        }

        btnHome?.setOnClickListener {
            showPatientList()
        }

        btnProfile?.setOnClickListener {
            showProfile()
        }

        leftMenu()

        var extras = intent.extras

        patientId = extras.getString("patientId")
        wholeName = extras.getString("wholeName")
        userEmail = extras.getString("userEmail")

        //Edit Text
        et_treatmentname = findViewById(R.id.et_treatmentname)
        et_startdate = findViewById(R.id.et_startdate)
        tv_time = findViewById(R.id.tv_time)

        //Button
        btn_addreminder = findViewById(R.id.btn_addreminder)
        btn_addanothertreatment = findViewById(R.id.btn_addanothertreatment)

        btn_addreminder?.setOnClickListener{
            val calendarDate = Calendar.getInstance()
            val currentDate = SimpleDateFormat("MMMM:dd:yyyy")
            dateToday = currentDate.format(calendarDate.time)

            val calendarTime = Calendar.getInstance()
            val currentTime = SimpleDateFormat("HH:mm:ss:SSS")

            timeToday = currentTime.format(calendarTime.time)

            treatmentName = et_treatmentname!!.text.toString()
            dateRecord = et_startdate!!.text.toString()
            timeRecord = tv_time!!.text.toString()

            treatmentReminderAdder.addTreatment(treatmentName, dateRecord, timeRecord, wholeName,
                patientId, dateToday, timeToday, this@AddATreatReminderActivity)
        }

        btn_addanothertreatment?.setOnClickListener{
            et_startdate!!.setText("")
            et_treatmentname!!.setText("")
            tv_time!!.setText("")
        }

        et_startdate?.setOnClickListener {
            DatePickerDialog(
                this@AddATreatReminderActivity, datePickerListener, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        tv_time?.setOnClickListener {
            timePicker()
        }
    }

    val datePickerListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(
            view: DatePicker, year: Int, monthOfYear: Int,
            dayOfMonth: Int
        ) {

            var today = Calendar.getInstance()
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val myFormat = "MM/dd/yy"
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            et_startdate?.setText(sdf.format(myCalendar.time))
        }
    }

    private fun timePicker() {
        val cal = Calendar.getInstance()
        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)
            tv_time?.text = SimpleDateFormat("hh:mm a").format(cal.time)
        }
        TimePickerDialog(this, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show()
    }

    override fun onResume() {
        super.onResume()
        userStatus.updateDoctorStatus("Online")

    }

    override fun onPause(){
        super.onPause()
        userStatus.updateDoctorStatus("Offline")
    }

    private fun leftMenu() {
        drawer {
            selectedItemByPosition = 5
            accountHeader {
                profile(wholeName!!, userEmail) {
                    icon = R.drawable.imgprofilepic
                }
                background = R.drawable.bgaccountheader
            }


            primaryItem("EVENTS") {
                icon = R.mipmap.ic_eventsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showEvents()
                    Log.d("DRAWER", "Events Activity")
                    true
                }
            }

            primaryItem("ABOUT") {
                icon = R.mipmap.ic_helpicon
                textColorRes = R.color.White
                onClick { _ ->
                    showAbout()
                    Log.d("DRAWER", "About Activity/Fragment")
                    true
                }
            }

            primaryItem("LOGOUT") {
                icon = R.mipmap.ic_logouticon
                textColorRes = R.color.White
                onClick { _ ->
                    logoutUser()
                    Log.d("DRAWER", "User Logout")
                    true
                }
            }

            sliderBackground = R.drawable.bgslider
        }
    }

    private fun logoutUser() {
        userStatus.updateDoctorStatus("Offline")
        firebaseConnection.userSignout()
        val intent = Intent(this@AddATreatReminderActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)


        return
    }

    private fun showAbout() {
        val intent = Intent(this@AddATreatReminderActivity, DoctorAboutActivity::class.java)
        startActivity(intent)
    }

    private fun showEvents() {
        val intent = Intent(this@AddATreatReminderActivity, DoctorEventsActivity::class.java)
        startActivity(intent)
    }

    private fun showProfile() {
        val intent = Intent(this@AddATreatReminderActivity, DoctorProfileActivity::class.java)
        startActivity(intent)
    }

    private fun showForum() {
        //val intent = Intent(this@DoctorEventsActivity, DoctorForumActivity::class.java)
        startActivity(intent)
    }

    private fun showPatientList() {
        val intent = Intent(this@AddATreatReminderActivity, PatientListActivity::class.java)
        startActivity(intent)
    }

}
