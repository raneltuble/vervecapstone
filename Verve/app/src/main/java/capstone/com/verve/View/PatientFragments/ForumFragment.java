package capstone.com.verve.View.PatientFragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import capstone.com.verve.API.FirebaseConnection;
import capstone.com.verve.Interface.AcceptListener;
import capstone.com.verve.Presenter.PostModel;
import capstone.com.verve.Presenter.Posts;
import capstone.com.verve.Presenter.UserPosts;
import capstone.com.verve.R;
import capstone.com.verve.View.PatientActivities.ForumActivity;
import capstone.com.verve.View.PatientActivities.ForumViewPostCommentsActivity;
import capstone.com.verve.View.PatientActivities.ProfileActivity;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.*;
import com.squareup.picasso.Picasso;
import org.w3c.dom.Text;

public class ForumFragment extends Fragment implements AcceptListener {

    RecyclerView mRecyclerView;

    public FirebaseRecyclerAdapter<PostModel, PostViewHolder> firebaseRecyclerAdapter;

    Query query;

    TextView txtName;
    TextView txtHearts;
    TextView txtComments;

    ImageView imgComment;
    ImageView img_heart;
    ImageView postImageView;

    DatabaseReference likesRef;
    DatabaseReference userRef;

    int countLikes;
    int countComments;

    String wholeName;
    String userEmail;


    Boolean likeChecker;


    FirebaseConnection firebaseConnection = new FirebaseConnection();

    String currentUserId = firebaseConnection.getCurrentUser();

    DatabaseReference postRef = firebaseConnection.getSpecificNodeReference("Posts");

    DatabaseReference commentRef;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ForumActivity activity = (ForumActivity) getActivity();

        wholeName = activity.getWholeName();
        userEmail = activity.getUserEmail();

        userRef = FirebaseDatabase.getInstance().getReference().child("Users");
        likesRef = firebaseConnection.getSpecificNodeReference("Likes");
        currentUserId = firebaseConnection.getCurrentUser();

        View rootView = inflater.inflate(R.layout.fragment_forum_following, container, false);
        mRecyclerView = rootView.findViewById(R.id.followingRecyclerView);
        mRecyclerView.hasFixedSize();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(layoutManager);

        return rootView;

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


    }

    public static class PostViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public PostViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setDetails(String firstname, String middlename,
                               String lastname, String postDescription,
                               String postTitle, String datePost,
                               String timePost, String postCateg,String photoUrl){

            //NAME
            TextView txtName = (TextView) mView.findViewById(R.id.txtName);
            String noMiddlename = "";
            String withMiddleName = "";

            if (middlename.isEmpty()) {
                noMiddlename = firstname.concat(" ".concat(lastname));
                txtName.setText(noMiddlename);
            } else {
                withMiddleName = firstname.concat(" ".concat(middlename.substring(0, 1)) + ". ".concat(lastname));
                txtName.setText(withMiddleName);
            }

            //POST DESCRIPTION
            TextView txtPostDetails = (TextView) mView.findViewById(R.id.txtPostDetails);
            txtPostDetails.setText(postDescription);

            //POST TITLE
            TextView txtPostTitle = mView.findViewById(R.id.txtPostTitle);
            txtPostTitle.setText(postTitle);

            //DATE POST
            TextView txtDate = mView.findViewById(R.id.txtDate);
            txtDate.setText(datePost);

            //TIME
            TextView txtTime = mView.findViewById(R.id.txtTime);
            txtTime.setText(timePost);

            //POST CATEGORY
            TextView txtCancerType = mView.findViewById(R.id.txtCancerType);
            txtCancerType.setText(postCateg);

            //IMAGE
            ImageView postImageView = mView.findViewById(R.id.postImageView);
            if (!photoUrl.isEmpty()) {
                Picasso.get().load(photoUrl).placeholder(R.drawable.bg_blur_hospital).into(postImageView);
            } else {
                postImageView.setVisibility(ImageView.GONE);
            }
        }
    }

    public void setLikeButtonStatus(final String key) {

        likesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(key).hasChild(currentUserId)) {
                    countLikes = (int) dataSnapshot.child(key).getChildrenCount();
                    //TODO: Red Heart
                    txtHearts.setText(Integer.toString(countLikes));
                } else {
                    countLikes = (int) dataSnapshot.child(key).getChildrenCount();
                    //TODO: Unliked Heart
                    txtHearts.setText(Integer.toString(countLikes));
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setCommentCount(String key, final TextView txtComments) {
        postRef.child(key).child("Comments").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    countComments = (int) dataSnapshot.getChildrenCount();
                    txtComments.setText(Integer.toString(countComments));
                } else {
                    txtComments.setText("0");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onStart() {

        super.onStart();

        query = firebaseConnection.getSpecificNodeReference("Posts").orderByChild("counter");

        FirebaseRecyclerOptions<PostModel> options = new FirebaseRecyclerOptions.Builder<PostModel>()
                .setQuery(query, PostModel.class).build();
        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<PostModel, PostViewHolder>(options) {
            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public int getItemViewType(int position) {
                return position;
            }


            @Override
            protected void onBindViewHolder(@NonNull PostViewHolder holder, int position, @NonNull PostModel model) {

                final String key = getRef(position).getKey();
                setCommentCount(key, txtComments);
                holder.setDetails(model.getFirstname(), model.getMiddlename(),
                        model.getLastname(), model.getPostDescription(),
                        model.getPostTitle(), model.getDatePost(),
                        model.getTimePost(), model.getPostCateg(),model.getPhotoUrl());


                txtName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        postRef.child(key).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String userId = dataSnapshot.child("uid").getValue(String.class);
                                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                                Bundle extras = new Bundle();
                                extras.putString("wholeName", wholeName);
                                extras.putString("userEmail", userEmail);
                                extras.putString("userId", userId);
                                intent.putExtras(extras);
                                startActivity(intent);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                });

                imgComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getActivity(), ForumViewPostCommentsActivity.class);
                        Bundle extras = new Bundle();
                        extras.putString("wholeName", wholeName);
                        extras.putString("userEmail", userEmail);
                        extras.putString("postId", key);
                        intent.putExtras(extras);
                        startActivity(intent);
                    }
                });

                img_heart.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        likeChecker = true;
                        likesRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                if (likeChecker.equals(false)) {
                                    if (dataSnapshot.child(key).hasChild(currentUserId)) {
                                        likesRef.child(key).child(currentUserId).removeValue();
                                        likeChecker = false;
                                    }
                                } else {
                                    likesRef.child(key).child(currentUserId).setValue(true);
                                    likeChecker = true;
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                });

            }

            @NonNull
            @Override
            public PostViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                final View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.item_post_forum, viewGroup, false);

                txtName = view.findViewById(R.id.txtName);
                imgComment = view.findViewById(R.id.img_comment);
                img_heart = view.findViewById(R.id.img_heart);
                txtHearts = view.findViewById(R.id.txtHearts);
                txtComments = view.findViewById(R.id.txtComments);
                postImageView = view.findViewById(R.id.postImageView);
                return new PostViewHolder(view);
            }
        };

        firebaseRecyclerAdapter.setHasStableIds(true);
        mRecyclerView.setAdapter(firebaseRecyclerAdapter);
        firebaseRecyclerAdapter.startListening();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //  firebaseRecyclerAdapter.stopListening();
    }

    @Override
    public void onResume() {
        super.onResume();
        //firebaseRecyclerAdapter.startListening();
    }

    @Override
    public void onPause() {
        super.onPause();
//        if (isRemoving() && mRecyclerView != null) {
//            mRecyclerView.removeAllViews();
//        }
    }

    @Override
    public void onSubmit() {

    }


}
