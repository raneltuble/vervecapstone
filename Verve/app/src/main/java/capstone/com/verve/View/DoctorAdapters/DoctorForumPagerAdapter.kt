package capstone.com.verve.View.DoctorAdapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import capstone.com.verve.View.DoctorFragments.DoctorForumFragment
import capstone.com.verve.View.PatientFragments.ForumFragment
import capstone.com.verve.View.PatientFragments.ForumPopularFragment

class DoctorForumPagerAdapter (fm: FragmentManager?, numOfTabs: Int) : FragmentPagerAdapter(fm) {

    private var numOfTabs: Int = numOfTabs
    private var homeFragment = DoctorForumFragment()
    private var popularFragment = ForumPopularFragment()

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return homeFragment
            1 -> return popularFragment
            else -> return null
        }    }

    override fun getCount(): Int {
        return numOfTabs
    }
}