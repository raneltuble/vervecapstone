package capstone.com.verve.View.DoctorActivities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.util.Log
import android.widget.ImageView
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Interface.AcceptListener
import capstone.com.verve.Presenter.UserStatus
import capstone.com.verve.R
import capstone.com.verve.View.DoctorAdapters.DoctorForumPagerAdapter
import capstone.com.verve.View.PatientActivities.LoginActivity
import capstone.com.verve.View.PatientFragments.ForumAddPostFrag
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.profile.profile
import kotlinx.android.synthetic.main.activity_doctor_forum.*

class DoctorForumActivity : AppCompatActivity(), AcceptListener {

    var btnBack: ImageView? = null
    var btnHome: ImageView? = null
    var btnProfile: ImageView? = null
    var btnForum: ImageView? = null
    var btnMessages: ImageView? = null

    var userEmail: String? = null
    var wholeName: String? = null
    internal var firebaseConnection = FirebaseConnection()
    var userStatus = UserStatus()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_forum)

        val extras = intent.extras
        wholeName = extras.getString("wholeName")
        userEmail = extras.getString("userEmail")

        btnForum = findViewById(R.id.img_forum)
        btnHome = findViewById(R.id.img_home)
        btnProfile = findViewById(R.id.img_profile)

        btnForum?.setOnClickListener {
            showForum()
        }

        btnHome?.setOnClickListener {
            showPatientList()
        }

        btnProfile?.setOnClickListener {
            showProfile()
        }

        click_fab.setOnClickListener {
            showPostDialog()
        }

        leftMenu()
        bindViewAndAdapter()
    }

    private fun bindViewAndAdapter() {
        val pageAdapter = DoctorForumPagerAdapter(supportFragmentManager, 2)
        viewpager.adapter = pageAdapter
        viewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
    }

    private fun showPostDialog() {
        val fm = supportFragmentManager
        var editNameDialogFragment = ForumAddPostFrag.newInstance("What's Up?")
        editNameDialogFragment.setListener(this@DoctorForumActivity)
        editNameDialogFragment.show(fm, "fragment_edit_name")
    }

    private fun leftMenu() {
        drawer {
            selectedItemByPosition = 5
            accountHeader {
                profile(wholeName!!, userEmail) {
                    icon = R.drawable.imgprofilepic
                }
                background = R.drawable.bgaccountheader
            }


            primaryItem("EVENTS") {
                icon = R.mipmap.ic_eventsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showEvents()
                    Log.d("DRAWER", "Events Activity")
                    true
                }
            }

            primaryItem("ABOUT") {
                icon = R.mipmap.ic_helpicon
                textColorRes = R.color.White
                onClick { _ ->
                    showAbout()
                    Log.d("DRAWER", "About Activity/Fragment")
                    true
                }
            }

            primaryItem("LOGOUT") {
                icon = R.mipmap.ic_logouticon
                textColorRes = R.color.White
                onClick { _ ->
                    logoutUser()
                    Log.d("DRAWER", "User Logout")
                    true
                }
            }

            sliderBackground = R.drawable.bgslider
        }
    }

    private fun logoutUser() {
        userStatus.updateDoctorStatus("Offline")
        firebaseConnection.userSignout()
        val intent = Intent(this@DoctorForumActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)


        return
    }

    private fun showAbout() {
        val intent = Intent(this@DoctorForumActivity, DoctorAboutActivity::class.java)
        startActivity(intent)
    }

    private fun showEvents() {
        val intent = Intent(this@DoctorForumActivity, DoctorEventsActivity::class.java)
        startActivity(intent)
    }

    private fun showProfile() {
        val intent = Intent(this@DoctorForumActivity, DoctorProfileActivity::class.java)
        startActivity(intent)
    }

    private fun showForum() {
        val intent = Intent(this@DoctorForumActivity, DoctorForumActivity::class.java)
        startActivity(intent)
    }

    private fun showPatientList() {
        val intent = Intent(this@DoctorForumActivity, PatientListActivity::class.java)
        startActivity(intent)
    }

    override fun onSubmit() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
