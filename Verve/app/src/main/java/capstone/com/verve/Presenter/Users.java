package capstone.com.verve.Presenter;

import android.os.Parcel;
import android.os.Parcelable;
import kotlinx.android.parcel.Parcelize;

import java.util.Date;
@Parcelize
public class Users implements Parcelable {

    public String firstname, middlename, lastname, username, mobile, email, address, birthday, age, gender, role, uid, religion;

    public String wholeName;


    public Users() {
    }

    public Users(String firstname, String middlename, String lastname, String username, String mobile,
                 String email, String address, String birthday,
                 String age, String gender, String role, String uid, String religion) {
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
        this.username = username;
        this.mobile = mobile;
        this.email = email;
        this.address = address;
        this.birthday = birthday;
        this.age = age;
        this.gender = gender;
        this.role = role;
        this.uid = uid;
        this.religion = religion;
    }

    protected Users(Parcel in) {
        firstname = in.readString();
        middlename = in.readString();
        lastname = in.readString();
        username = in.readString();
        mobile = in.readString();
        email = in.readString();
        address = in.readString();
        birthday = in.readString();
        age = in.readString();
        gender = in.readString();
        role = in.readString();
        uid = in.readString();
        religion = in.readString();
        wholeName = in.readString();
    }

    public static final Creator<Users> CREATOR = new Creator<Users>() {
        @Override
        public Users createFromParcel(Parcel in) {
            return new Users(in);
        }

        @Override
        public Users[] newArray(int size) {
            return new Users[size];
        }
    };

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getWholeName() {
        return wholeName;
    }

    public void setWholeName(String wholeName) {
        this.wholeName = wholeName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstname);
        dest.writeString(middlename);
        dest.writeString(lastname);
        dest.writeString(username);
        dest.writeString(mobile);
        dest.writeString(email);
        dest.writeString(address);
        dest.writeString(birthday);
        dest.writeString(age);
        dest.writeString(gender);
        dest.writeString(role);
        dest.writeString(uid);
        dest.writeString(religion);
        dest.writeString(wholeName);
    }
}
