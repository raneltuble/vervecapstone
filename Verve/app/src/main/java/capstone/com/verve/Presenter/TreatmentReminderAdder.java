package capstone.com.verve.Presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;
import capstone.com.verve.API.FirebaseConnection;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class TreatmentReminderAdder {
    FirebaseConnection firebaseConnection = new FirebaseConnection();


    public void addTreatment(String treatmentName, String date, String time, String doctorName, String uid,
                             String dateReference, String timeReference, final Context context){
        TreatmentReminders treatmentReminders = new TreatmentReminders(treatmentName, date, time, doctorName, uid);
        firebaseConnection.getSpecificNodeReference("TreatmentReminder").child(uid).child(uid.concat(dateReference.concat(timeReference))).setValue(treatmentReminders).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(context, "Treatment Reminder Added Successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
