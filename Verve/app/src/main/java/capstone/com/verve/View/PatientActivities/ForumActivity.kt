package capstone.com.verve.View.PatientActivities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import capstone.com.verve.Interface.AcceptListener
import capstone.com.verve.R
import capstone.com.verve.View.PatientAdapters.ForumPagerAdapter
import capstone.com.verve.View.PatientFragments.ForumAddPostFragment
import kotlinx.android.synthetic.main.activity_forum.*
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View.OnTouchListener
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.View.DirectMessages.LatestMessagesActivity
import capstone.com.verve.View.PatientFragments.ForumAddPostFrag
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.badgeable.secondaryItem
import co.zsmb.materialdrawerkt.draweritems.divider
import co.zsmb.materialdrawerkt.draweritems.profile.profile


class ForumActivity : BaseView(), AcceptListener {
    var searchBar: EditText? = null
    var wholeName: String = ""
    var userEmail: String = ""
    internal var firebaseConnection = FirebaseConnection()
    var userId: String = "";


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forum)

        var extras = intent.extras

        wholeName = extras.getString("wholeName")
        userEmail = extras.getString("userEmail")
//        userId = extras.getString("userId")

        var imgFind = findViewById<ImageView>(R.id.img_find)
        var imgReminders = findViewById<ImageView>(R.id.img_reminders)
        var img_home = findViewById<ImageView>(R.id.img_home)
        var imgProfile = findViewById<ImageView>(R.id.img_profile)
        var imgMessages = findViewById<ImageView>(R.id.img_messages)
        var tabLayout = findViewById<View>(R.id.tabLayout)
        searchBar = findViewById(R.id.searchBar)

        leftMenu()
        bindViewAndAdapter()

        click_fab.setOnClickListener {
            showPostDialog()
        }

        //BOTTOM NAVIGATION REGION
        imgProfile.setOnClickListener {
            showProfile()
        }

        imgFind.setOnClickListener {
            showFind()
        }

        imgReminders.setOnClickListener {
            showReminders()
        }

        imgMessages.setOnClickListener {
            showMessages()
        }
        //BOTTOM NAVIGATION REGION

        //SEARCH REGION
        btn_search.setOnClickListener {
            showSearchBar()
        }
        showSearchBarResults()
        closeSearchBar()
        //SEARCH REGION
    }

    private fun bindViewAndAdapter() {
        val pageAdapter = ForumPagerAdapter(supportFragmentManager, 2)
        viewpager.adapter = pageAdapter
        viewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
    }

    private fun showPostDialog() {
        val fm = supportFragmentManager
        var editNameDialogFragment = ForumAddPostFrag.newInstance("What's Up?")
        editNameDialogFragment.setListener(this@ForumActivity)
        editNameDialogFragment.show(fm, "fragment_edit_name")
    }

    private fun showProfile() {
        val intent = Intent(this@ForumActivity, ProfileActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showFind() {
        val intent = Intent(this@ForumActivity, FindActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showReminders() {
        val intent = Intent(this@ForumActivity, MedRemindersActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showMessages() {
        val intent = Intent(this@ForumActivity, LatestMessagesActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }


    private fun showSearchBar() {
        searchBar?.visibility = View.VISIBLE
        btn_menu.visibility = View.INVISIBLE
        txt_vervetop.visibility = View.INVISIBLE
        btn_search.visibility = View.INVISIBLE
    }

    private fun showSearchBarResults() {
        searchBar?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
              //  forumAdapter?.getFilter()?.filter(editable.toString())
            }
        })
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun closeSearchBar() {
        searchBar?.setOnTouchListener(OnTouchListener { v, event ->
            val DRAWABLE_LEFT = 0
            val DRAWABLE_TOP = 1
            val DRAWABLE_RIGHT = 2
            val DRAWABLE_BOTTOM = 3

            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= searchBar?.right!! - searchBar?.compoundDrawables!![DRAWABLE_RIGHT].bounds.width()) {
                    searchBar?.visibility = View.INVISIBLE
                    searchBar?.hideSoftKeyboardOnFocusLostEnabled(true)
                    btn_menu.visibility = View.VISIBLE
                    txt_vervetop.visibility = View.VISIBLE
                    btn_search.visibility = View.VISIBLE

                    return@OnTouchListener true
                }
            }
            false
        })
    }

    private fun leftMenu() {
        drawer {
            selectedItemByPosition = 9
            accountHeader {
                profile(wholeName, userEmail) {
                    icon = R.drawable.imgprofilepic
                }
                background = R.drawable.bgaccountheader
            }

            primaryItem("HEALTH TIPS") {
                icon = R.mipmap.ic_tipsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showHealthTips()
                    Log.d("DRAWER", "Health Tips Activity")
                    true
                }
            }

            primaryItem("EVENTS") {
                icon = R.mipmap.ic_eventsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showEvents()
                    Log.d("DRAWER", "Events Activity")
                    true
                }
            }

            primaryItem("ABOUT") {
                icon = R.mipmap.ic_helpicon
                textColorRes = R.color.White
                onClick { _ ->
                    showAbout()
                    Log.d("DRAWER", "About Activity/Fragment")
                    true
                }
            }

            primaryItem("MANAGE PRIVACY") {
                icon = R.mipmap.ic_settings
                textColorRes = R.color.White
                onClick { _ ->
                    showManagePrivacy()
                    Log.d("DRAWER", "Manage Privacy Activity")
                    true
                }
            }

            primaryItem("LOGOUT") {
                icon = R.mipmap.ic_logouticon
                textColorRes = R.color.White
                onClick { _ ->
                    logoutUser()
                    Log.d("DRAWER", "User Logout")
                    true
                }
            }

            sliderBackground = R.drawable.bgslider
        }
    }

    private fun logoutUser() {
        firebaseConnection.firebaseAuth.signOut()
        val intent = Intent(this@ForumActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    private fun showAbout() {
        val intent = Intent(this@ForumActivity, PatientAboutActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showManagePrivacy() {
        val intent = Intent(this@ForumActivity, ManagePrivacyActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun showEvents() {
        //val intent = Intent(this@ForumActivity, PatientEventsActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)

        val intent = Intent(this@ForumActivity, PatientEventsActivity::class.java)
        startActivity(intent)
    }

    private fun showHealthTips() {
        val intent = Intent(this@ForumActivity, PatientHealthTipsActivity::class.java)
        var extras = Bundle()
        extras.putString("wholeName", wholeName)
        extras.putString("userEmail", userEmail)
        extras.putString("userId", userId)
        intent.putExtras(extras)
        startActivity(intent)
    }


    override fun onSubmit() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
