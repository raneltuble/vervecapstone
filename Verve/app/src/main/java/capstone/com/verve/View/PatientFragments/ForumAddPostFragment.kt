package capstone.com.verve.View.PatientFragments

import android.app.Activity
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Interface.AcceptListener
import capstone.com.verve.Presenter.Posts
import capstone.com.verve.R
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import org.jetbrains.anko.find
import java.text.SimpleDateFormat
import java.util.*


class ForumAddPostFragment : DialogFragment(){

    private var userRef: DatabaseReference? = null

    private var postLog: DatabaseReference? = null

    private var auth: FirebaseAuth? = null

    private var postTitle: EditText? = null

    private var descTitle: EditText? = null

    private var acceptButton: Button? = null

    private var cancelButton: Button? = null

    private var spinner: Spinner? = null

    private var postRef: DatabaseReference? = null

    internal var firebaseConnection = FirebaseConnection()

    var storageRef = FirebaseStorage.getInstance().reference

    val GALLERY_PICK: Int = 0

    var btn_addimage: ImageView? = null

    var imageUri: Uri? = null

    var saveCurrentDate: String? = null
    var saveCurrentTime: String? = null
    var postRandomName: String? = null

    // private var postList: ArrayList<SampleData> = ArrayList()
    private var listener: OnFragmentInteractionListener? = null

    val storageReference = FirebaseStorage.getInstance().reference

    var posts: Posts = Posts()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            //RecyclerView
        }


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_add_post, container, false)

        postTitle = rootView.findViewById(R.id.txt_your_name)
        descTitle = rootView.findViewById(R.id.txt_your_num)
        acceptButton = rootView.findViewById(R.id.buttonAccept)
        cancelButton = rootView.findViewById(R.id.buttonCancel)
        spinner = rootView.findViewById(R.id.spinner)
        btn_addimage = rootView.findViewById(R.id.btn_addimage)

        userRef = firebaseConnection.getSpecificNodeReference("Users")
        postRef = firebaseConnection.getSpecificNodeReference("Posts")
        postLog = firebaseConnection.getSpecificNodeReference("PostLogs")
        auth = firebaseConnection.firebaseAuth

        acceptButton?.setOnClickListener {

            var calendarDate = Calendar.getInstance()
            var currentDate = SimpleDateFormat("dd-MMMM-yyyy")
            saveCurrentDate = currentDate.format(calendarDate.time)

            var calendarTime = Calendar.getInstance()
            var currentTime = SimpleDateFormat("HH:mm:ss:SSS")
            saveCurrentDate = currentDate.format(calendarTime.time)
            saveCurrentTime = currentTime.format(calendarTime.time)

            postRandomName = saveCurrentDate + saveCurrentTime

            if (imageUri != null) {
                var filePath: StorageReference =
                    storageRef.child("Post Image/${imageUri?.lastPathSegment + ".jpg"}")
                var uploadTask =  filePath.putFile(imageUri!!)

                filePath.putFile(imageUri!!).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        filePath.putFile(imageUri!!)
                            .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                                if (!task.isSuccessful) {
                                    task.exception?.let {
                                        throw it
                                    }
                                }
                                return@Continuation filePath.downloadUrl
                            }).addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    val downloadUri = task.result.toString()
                                    Toast.makeText(context, "Success", Toast.LENGTH_LONG).show()
                                } else {
                                    // Handle failures
                                    // ...
                                }
                            }

                    }
                }
                //posts.savePosts(userRef, postRef, postLog, auth, postTitle, descTitle, spinner, activity!!.applicationContext)


            }
            dismiss()
        }

        btn_addimage?.setOnClickListener {

            openGallery()

        }

        cancelButton?.setOnClickListener {
            dismiss()
        }

        return rootView
    }

    override fun onResume() {
        super.onResume()

        val width = resources.getDimensionPixelSize(R.dimen.medfragment)
        dialog.window!!.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT)
    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    fun openGallery() {
        var galleryIntent = Intent(
            Intent.ACTION_PICK,
            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        galleryIntent.type = "image/*"
        startActivityForResult(galleryIntent, GALLERY_PICK)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GALLERY_PICK && resultCode == Activity.RESULT_OK && data != null) {
            imageUri = data.data

        }
    }

    lateinit var acceptListen: AcceptListener
    fun setListener(accept: AcceptListener) {
        acceptListen = accept
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

//    override fun onSubmit() {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }

    companion object {
        fun newInstance(title: String): ForumAddPostFragment {
            val frag = ForumAddPostFragment()
            val args = Bundle()
            args.putString("title", title)
            frag.arguments = args
            return frag
        }
    }

}
