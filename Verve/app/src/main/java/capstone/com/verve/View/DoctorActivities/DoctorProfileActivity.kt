package capstone.com.verve.View.DoctorActivities

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import capstone.com.verve.R
import android.support.design.widget.TabLayout
import android.util.Log
import capstone.com.verve.View.DoctorAdapters.DoctorProfilePagerAdapter
import kotlinx.android.synthetic.main.activity_doctor_profile.*
import android.view.ViewGroup
import android.widget.ImageView
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.UserStatus
import capstone.com.verve.View.PatientActivities.LoginActivity
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.profile.profile


class DoctorProfileActivity : AppCompatActivity() {

    var btnBack: ImageView? = null
    var btnHome: ImageView? = null
    var btnProfile: ImageView? = null
    var btnForum: ImageView? = null
    var btnMessages: ImageView? = null

    var userEmail: String? = null
    var wholeName: String? = null
    internal var firebaseConnection = FirebaseConnection()
    var userStatus = UserStatus()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_profile)

        val extras = intent.extras
        wholeName = extras.getString("wholeName")
        userEmail = extras.getString("userEmail")

        btnForum = findViewById(R.id.img_forum)
        btnHome = findViewById(R.id.img_home)

        btnForum?.setOnClickListener {
            showForum()
        }

        btnHome?.setOnClickListener {
            showHome()
        }

        leftMenu()

        bindViewAndAdapter()
        tabSpacing()

    }

    override fun onResume() {
        super.onResume()
        userStatus.updateDoctorStatus("Online")

    }

    override fun onPause() {
        super.onPause()
        userStatus.updateDoctorStatus("Offline")
    }

    private fun showHome() {
        val intent = Intent(this@DoctorProfileActivity, PatientListActivity::class.java)
        var extras = Bundle()
        extras.putString("userEmail", userEmail)
        extras.putString("wholeName", wholeName)
        intent.putExtras(extras)
        startActivity(intent)
    }

    private fun bindViewAndAdapter() {
        val pageAdapter = DoctorProfilePagerAdapter(supportFragmentManager, 3)
        viewpager.adapter = pageAdapter
        viewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
    }

    private fun tabSpacing() {
        for (i in 0 until tabLayout.tabCount) {
            val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(10, 0, 10, 0)
            tab.requestLayout()
        }
    }

    private fun leftMenu() {
        drawer {
            selectedItemByPosition = 5
            accountHeader {
                profile(wholeName!!, userEmail) {
                    icon = R.drawable.imgprofilepic
                }
                background = R.drawable.bgaccountheader
            }


            primaryItem("EVENTS") {
                icon = R.mipmap.ic_eventsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showEvents()
                    Log.d("DRAWER", "Events Activity")
                    true
                }
            }

            primaryItem("ABOUT") {
                icon = R.mipmap.ic_helpicon
                textColorRes = R.color.White
                onClick { _ ->
                    showAbout()
                    Log.d("DRAWER", "About Activity/Fragment")
                    true
                }
            }

            primaryItem("LOGOUT") {
                icon = R.mipmap.ic_logouticon
                textColorRes = R.color.White
                onClick { _ ->
                    logoutUser()
                    Log.d("DRAWER", "User Logout")
                    true
                }
            }

            sliderBackground = R.drawable.bgslider
        }
    }

    private fun logoutUser() {
        userStatus.updateDoctorStatus("Offline")
        firebaseConnection.userSignout()
        val intent = Intent(this@DoctorProfileActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)


        return
    }

    private fun showAbout() {
        val intent = Intent(this@DoctorProfileActivity, DoctorAboutActivity::class.java)
        startActivity(intent)
    }

    private fun showEvents() {
        val intent = Intent(this@DoctorProfileActivity, DoctorEventsActivity::class.java)
        startActivity(intent)
    }

    private fun showProfile() {
        val intent = Intent(this@DoctorProfileActivity, DoctorProfileActivity::class.java)
        startActivity(intent)
    }

    private fun showForum() {
        val intent = Intent(this@DoctorProfileActivity, DoctorForumActivity::class.java)
        startActivity(intent)
    }

//    private fun showPatientList() {
//        val intent = Intent(this@DoctorProfileActivity, PatientListActivity::class.java)
//        startActivity(intent)
//    }
}
