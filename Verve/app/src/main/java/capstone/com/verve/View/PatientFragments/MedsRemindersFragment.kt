package capstone.com.verve.View.PatientFragments

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.LinearLayout
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.MedReminders
import capstone.com.verve.R
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.item_reminders_meds.view.*
import capstone.com.verve.Interface.AcceptListener


class MedsRemindersFragment : Fragment(), AcceptListener {

    lateinit var mMedsViewHolder: FirebaseRecyclerAdapter<MedReminders, MedsViewHolder>
    lateinit var mRecyclerView: RecyclerView
    lateinit var mDatabase: DatabaseReference
    private var auth: FirebaseAuth? = null
    internal var firebaseConnection = FirebaseConnection()

    var medItem: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView = inflater.inflate(R.layout.fragment_meds_reminders, container, false)
        mRecyclerView = rootView.findViewById(R.id.rv_medrem)

        return rootView
    }

    override fun onStart() {
        super.onStart()
        mMedsViewHolder.startListening()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        mRecyclerView.hasFixedSize()
        var layoutManager = LinearLayoutManager(context)
        layoutManager.reverseLayout = true
        layoutManager.stackFromEnd = true
        mRecyclerView.layoutManager = layoutManager

        mDatabase = FirebaseDatabase.getInstance().reference
        val medsQuery = mDatabase.child("") //TODO: Database
        val medsOptions = FirebaseRecyclerOptions.Builder<MedReminders>().setQuery(medsQuery, MedReminders::class.java).build()

        mMedsViewHolder = object : FirebaseRecyclerAdapter<MedReminders, MedsViewHolder>(medsOptions) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedsViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_reminders_meds, parent, false)

                auth = FirebaseAuth.getInstance()
                medItem = view.findViewById(R.id.item_medicine)

                medItem?.setOnClickListener {
                    showContraindicationsDialog()
                }

                return MedsViewHolder(view)
            }

            override fun onBindViewHolder(holder: MedsViewHolder, position: Int, model: MedReminders) {
                holder.bind(model)
            }
        }

        mRecyclerView.adapter = mMedsViewHolder
    }

    fun showContraindicationsDialog() {
        val fm = fragmentManager
        var contraindicationsFragment = PatientContraindicationsFragment.newInstance("Contraindications")
        contraindicationsFragment.setListener(this@MedsRemindersFragment)
        contraindicationsFragment.show(fm, "fragment_contraindications")

    }

    class MedsViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        fun bind(meds: MedReminders) = with(itemView) {
            //TODO: MODEL
            txt_times?.text = meds.medsName
            txt_medstime?.text = meds.medsDate
            txt_medsfreq?.text = meds.medsFreq
            txt_medsdays?.text = meds.medsDays
        }
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MedsRemindersFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

    override fun onSubmit() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
