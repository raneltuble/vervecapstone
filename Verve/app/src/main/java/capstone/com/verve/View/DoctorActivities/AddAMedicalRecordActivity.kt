package capstone.com.verve.View.DoctorActivities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import capstone.com.verve.API.FirebaseConnection
import capstone.com.verve.Presenter.MedicalRecordAdder
import capstone.com.verve.Presenter.UserStatus
import capstone.com.verve.R
import capstone.com.verve.View.PatientActivities.ForumActivity
import capstone.com.verve.View.PatientActivities.LoginActivity
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.profile.profile
import java.text.SimpleDateFormat
import java.util.*

class AddAMedicalRecordActivity : AppCompatActivity() {
    var userEmail: String? = null
    var wholeName: String? = null
    var dateToday: String? = null
    var timeToday: String? = null
    var patientId: String? = null
    var dateTodayMedRecord: String? = null
    var currentTimeRecord: String? = null

    //EditText
    var et_treatment: EditText? = null
    var et_bp: EditText? = null
    var et_findings: EditText? = null
    var et_diagnosis: EditText? = null
    var et_prescriptions: EditText? = null

    internal var firebaseConnection = FirebaseConnection()

    var btn_addmedrecord: Button? = null

    //String getText
    var treatment: String? = null
    var bp: String? = null
    var findings: String? = null
    var diagnosis: String? = null
    var prescription: String? = null


    var medicalRecord = MedicalRecordAdder()
    var userStatus = UserStatus()

    var btnBack: ImageView? = null
    var btnHome: ImageView? = null
    var btnProfile: ImageView? = null
    var btnForum: ImageView? = null
    var btnMessages: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_amedical_record)

        var extras = intent.extras

        patientId = extras.getString("patientId")
        wholeName = extras.getString("wholeName")
        userEmail = extras.getString("userEmail")

        et_treatment = findViewById(R.id.et_treatment)
        et_bp = findViewById(R.id.et_bp)
        et_diagnosis = findViewById(R.id.et_diagnosis)
        et_findings = findViewById(R.id.et_diagnosis)
        et_prescriptions = findViewById(R.id.et_prescriptions)

        //Get Text

        btnBack = findViewById(R.id.btn_back)
        btnForum = findViewById(R.id.img_forum)
        btnHome = findViewById(R.id.img_home)
        btn_addmedrecord = findViewById(R.id.btn_addmedrecord)
        btnProfile = findViewById(R.id.img_profile)

        btnBack?.setOnClickListener {
            showPatientList()
        }

        btnForum?.setOnClickListener {
            showForum()
        }

        btnHome?.setOnClickListener {
            showPatientList()
        }

        btnProfile?.setOnClickListener {
            showProfile()
        }

        leftMenu()

        btn_addmedrecord?.setOnClickListener{
            val calendarDate = Calendar.getInstance()
            val currentDate = SimpleDateFormat("MMMM:dd:yyyy")
            dateToday = currentDate.format(calendarDate.time)

            val calendarTime = Calendar.getInstance()
            val currentTime = SimpleDateFormat("HH:mm:ss:SSS")
            timeToday = currentTime.format(calendarTime.time)

            val timeRecord = Calendar.getInstance()
            val timeFormat = SimpleDateFormat("hh:mm a")
            currentTimeRecord = timeFormat.format(timeRecord.time)

            val recordDate = Calendar.getInstance()
            val currentRecordDate =  SimpleDateFormat("MM/dd/yyyy");
            dateTodayMedRecord = currentRecordDate.format(recordDate.time)

            treatment = et_treatment!!.text.toString()
            bp = et_bp!!.text.toString()
            diagnosis = et_diagnosis!!.text.toString()
            findings = et_findings!!.text.toString()
            prescription = et_prescriptions!!.text.toString()
            medicalRecord.addMedicalRecord(patientId, treatment, bp, findings, diagnosis,
                prescription, wholeName, dateToday, timeToday, dateTodayMedRecord, currentTimeRecord,this@AddAMedicalRecordActivity)

        }
    }

    private fun leftMenu() {
        drawer {
            selectedItemByPosition = 5
            accountHeader {
                profile(wholeName!!, userEmail) {
                    icon = R.drawable.imgprofilepic
                }
                background = R.drawable.bgaccountheader
            }


            primaryItem("EVENTS") {
                icon = R.mipmap.ic_eventsicon
                textColorRes = R.color.White
                onClick { _ ->
                    showEvents()
                    Log.d("DRAWER", "Events Activity")
                    true
                }
            }

            primaryItem("ABOUT") {
                icon = R.mipmap.ic_helpicon
                textColorRes = R.color.White
                onClick { _ ->
                    showAbout()
                    Log.d("DRAWER", "About Activity/Fragment")
                    true
                }
            }

            primaryItem("LOGOUT") {
                icon = R.mipmap.ic_logouticon
                textColorRes = R.color.White
                onClick { _ ->
                    logoutUser()
                    Log.d("DRAWER", "User Logout")
                    true
                }
            }

            sliderBackground = R.drawable.bgslider
        }
    }

    private fun logoutUser() {
        userStatus.updateDoctorStatus("Offline")
        firebaseConnection.userSignout()
        val intent = Intent(this@AddAMedicalRecordActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)


        return
    }

    private fun showAbout() {
        val intent = Intent(this@AddAMedicalRecordActivity, DoctorAboutActivity::class.java)
        startActivity(intent)
    }

    private fun showEvents() {
        val intent = Intent(this@AddAMedicalRecordActivity, DoctorEventsActivity::class.java)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        userStatus.updateDoctorStatus("Online")

    }

    override fun onPause(){
        super.onPause()
        userStatus.updateDoctorStatus("Offline")
    }

    private fun showProfile() {
        val intent = Intent(this@AddAMedicalRecordActivity, DoctorProfileActivity::class.java)
        startActivity(intent)
    }

    private fun showForum() {
        val intent = Intent(this@AddAMedicalRecordActivity, DoctorForumActivity::class.java)
        startActivity(intent)
    }

    private fun showPatientList() {
        val intent = Intent(this@AddAMedicalRecordActivity, PatientListActivity::class.java)
        startActivity(intent)
    }
}
