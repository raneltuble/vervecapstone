package capstone.com.verve.Presenter;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.*;
import capstone.com.verve.API.FirebaseConnection;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public class ManagePrivacy {

    String switchAge = "";
    String switchGender = "";
    String switchBday = "";
    String switchReligion = "";
    String switchIcon = "";
    String switchEmail = "";
    String switchAddress = "";
    String switchMobilenum = "";

    String switchTrue = "true";
    String switchFalse = "false";

    FirebaseConnection firebaseConnection = new FirebaseConnection();

    public void updatePrivacy(String data, String status) {
        firebaseConnection.getSpecificNodeReference("ManagePrivacy")
                .child(firebaseConnection.getCurrentUser()).child(data).setValue(status)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {

                        } else {

                        }
                    }
                });
    }

    public void setChecked(final Switch age, final Switch address,
                           final Switch bday, final Switch email,
                           final Switch gender, final Switch icon,
                           final Switch religion, final Switch mobileNum) {

        firebaseConnection.getSpecificNodeReference("ManagePrivacy")
                .child(firebaseConnection.getCurrentUser()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    switchAge = dataSnapshot.child("age").getValue(String.class);
                    switchAddress = dataSnapshot.child("address").getValue(String.class);
                    switchBday = dataSnapshot.child("bday").getValue(String.class);
                    switchEmail = dataSnapshot.child("email").getValue(String.class);
                    switchIcon = dataSnapshot.child("icon").getValue(String.class);
                    switchGender = dataSnapshot.child("gender").getValue(String.class);
                    switchMobilenum = dataSnapshot.child("mobilenum").getValue(String.class);
                    switchReligion = dataSnapshot.child("religion").getValue(String.class);

                    if (switchAge.equals(switchTrue)) {
                        age.setChecked(true);
                    } else {
                        age.setChecked(false);
                    }
                    if (switchEmail.equals(switchTrue)) {
                        email.setChecked(true);
                    } else {
                        email.setChecked(false);
                    }
                    if (switchAddress.equals(switchTrue)) {
                        address.setChecked(true);
                    } else {
                        address.setChecked(false);
                    }
                    if (switchMobilenum.equals(switchTrue)) {
                        mobileNum.setChecked(true);
                    } else {
                        mobileNum.setChecked(false);
                    }
                    if (switchGender.equals(switchTrue)) {
                        gender.setChecked(true);
                    } else {
                        gender.setChecked(false);
                    }
                    if (switchIcon.equals(switchTrue)) {
                        icon.setChecked(true);
                    } else {
                        icon.setChecked(false);
                    }
                    if (switchReligion.equals(switchTrue)) {
                        religion.setChecked(true);
                    } else {
                        switchReligion.equals(false);
                    }

                }else{

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    public void setVisible(final TextView age, final TextView address,
                           final TextView bday, final TextView email,
                           final TextView religion, final TextView gender,
                           ImageView edit, ImageView settings, String userId) {

        edit.setVisibility(ImageView.INVISIBLE);
        settings.setVisibility(ImageView.INVISIBLE);

        firebaseConnection.getSpecificNodeReference("ManagePrivacy")
                .child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    switchAge = dataSnapshot.child("age").getValue(String.class);
                    switchAddress = dataSnapshot.child("address").getValue(String.class);
                    switchEmail = dataSnapshot.child("email").getValue(String.class);
                    switchIcon = dataSnapshot.child("icon").getValue(String.class);
                    switchGender = dataSnapshot.child("gender").getValue(String.class);
                    switchMobilenum = dataSnapshot.child("mobilenum").getValue(String.class);
                    switchReligion = dataSnapshot.child("religion").getValue(String.class);

                    if (switchAge.equals(switchTrue)) {
                        age.setVisibility(TextView.VISIBLE);
                        bday.setVisibility(TextView.VISIBLE);

                    } else {
                        age.setVisibility(TextView.GONE);
                        bday.setVisibility(TextView.GONE);

                    }
                    if (switchEmail.equals(switchTrue)) {
                        email.setVisibility(TextView.VISIBLE);
                    } else {
                        email.setVisibility(TextView.GONE);
                    }
                    if (switchAddress.equals(switchTrue)) {
                        address.setVisibility(TextView.VISIBLE);
                    } else {
                        address.setVisibility(TextView.GONE);
                    }
//                    if (switchMobilenum.equals(switchTrue)) {
//                        mobileNum.setVisibility(TextView.VISIBLE);
//                    } else {
//                        mobileNum.setVisibility(TextView.INVISIBLE);
//                    }
                    if (switchGender.equals(switchTrue)) {
                        gender.setVisibility(TextView.VISIBLE);
                    } else {
                        gender.setVisibility(TextView.INVISIBLE);
                    }
//                    if (switchIcon.equals(switchTrue)) {
//                        icon.setVisibility(TextView.VISIBLE);
//                    } else {
//                        icon.setVisibility(TextView.INVISIBLE);
//                    }
                    if (switchReligion.equals(switchTrue)) {
                        religion.setVisibility(TextView.VISIBLE);
                    } else {
                        religion.setVisibility(TextView.GONE);
                    }

                }else{

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}